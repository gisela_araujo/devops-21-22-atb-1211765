# Class Assignment 3 Report - Part 1

### Virtualization with Vagrant

## 1. Analysis, Design and Implementation

First, I downloaded VirtualBox into my computer through [here](https://www.virtualbox.org/wiki/Downloads).

Another important subject to refer is the issues created for this Class Assignment, 
which were the following ones:

* Issue #30 : Create a VM as described in the lecture;
* Issue #31 : Clone individual repository inside the VM;
* Issue #32 : Build and Execute the spring boot projects from the previous assignments;
* Issue #33 : Create gradle task for runClient in VM.



### 1.1 Create a VM as described in the lecture

In order to create my Virtual Machine I followed all the steps from the lecture
and didn't encounter any difficulties or problems.

The only change was that, instead of installing the JDK version 8, I installed the version 11.

Install VM Virtual Box:

![OracleInstallation](../PART1/images/Oracle_VM_Virtual_Box_Installation.jpg)

Create VM in Virtual Box Manager:

![createVM](../PART1/images/createVM.jpg)

Install Ubuntu:

![InstallUbuntu](../PART1/images/InstallUbuntu.jpg)

Ubuntu Running in VM:

![UbuntuRunning](../PART1/images/ubuntuVMRunning.jpg)


### 1.2 Clone your individual repository inside the VM

To clone my individual repository inside the VM I used the following command:

    git clone https://Gisela_Araujo@bitbucket.org/gisela_araujo/devops-21-22-atb-1211765.git

![cloneMyRepository](../PART1/images/cloneMyRepository.jpg)


### 1.3 Build and Execute the spring boot tutorial basic project and the gradle_basic_demo project (from the previous assignments)

Then I cloned the Spring Tutorial application, for this I used the following command:

    git clone https://github.com/spring-guides/tut-react-and-spring-data-rest.git


Then I built the application using the command:

    ./mvnw spring-boot:run


![buildTutorial](../PART1/images/buildSpringTutorial.jpg)


After the build of the application was done successfully and, in order to see if everything was ok, in the host computer with the url - **http://192.168.56.5:8080/** - I could see the expected result, as seen below:

![buildLocalHostWorking](../PART1/images/localhostWorking.jpg)


Then, and as needed I installed the dependencies that were necessary for the project.

First, I installed maven:

    sudo apt install maven


Then, it was time to install gradle:

    sudo apt install gradle


#### 1.3.1. Previous Assignments : Build CA1

After installing the dependencies it was time to build and run the first Class Assignment inside the VM and
see if everything was working as expected. For this I run the following command:

    mvn spring-boot:run

as can be seen below everything worked as expected and the build was successful:


![mavenSpringBootRun](../PART1/images/CA1_maven_SpringBootRun_Successfully.jpg)


#### 1.3.2. Previous Assignments : Build CA2

#### 1.3.2.1 CA2 Part One:

For the second Class Assignment I divided what I did into two parts as it was mandatory to do previously.
Both these parts were dedicated to using gradle and building a project using this.

First, I used the command:

        ./gradlew build

but for this I encounter some permission issues, as can be seen below,

![permissãoNegada](../PART1/images/permissãoNegadaGradlewBuild.jpg)

so with the following command I got the permissions to run gradle,

        chmod u+x gradlew

After this I had no problems running the command for building, as mentioned above.
But, I encounter another problem which didn't allow me to run the Server.
To resolve this problem I had to commit the folder wrapper, and with it the
*gradle-wrapper.jar*. After this everything worked as expected.

So, in my VM I run the server with the command:

        ./gradlew runServer

I also added a new task for running the chat with the VM, because I encounter this problem,

![buildFailedForRunClient](../PART1/images/buildFailedForRunClient.jpg)

This error appeared because I was using a code dependent on display which was not able to occur
using the VM. So I had  to run the code in my Windows terminal and create a new task
in gradle:

    task runClientVM(type:JavaExec, dependsOn: classes){

        group = "DevOps"
        description = "Launches a chat client that connects to a server on 192.168.56.5 "

        classpath = sourceSets.main.runtimeClasspath

        main = 'basic_demo.ChatClientApp'

            args '192.168.56.5', '59001'
    }

On Windows I run this task,

![runClientVM](../PART1/images/runClientVMForOpeningChatWithVM.jpg)

as can be seen below everything worked as expected and the chat was working,

![chatRunningInVM](../PART1/images/chatIsRunningInVM.jpg)

#### 1.3.2.2 CA2 Part Two:

For the second part of CA2 I ran, as usual, the command

      ./gradlew build


and everything worked successfully,

![BuildSuccessful](../PART1/images/CA2_BuildSuccessful.jpg)

After the build, I verified that the folder ***built*** had two files in it,
one was the *bundle.js* and the other was *bundle.js.map*, as illustrated below,

![TreeBuiltStillExists](../PART1/images/treeBuiltStillExists.jpg)


To empty the folder ***built*** I ran the task I created for the CA2-part2:

    task deleteFilesGeneratedByWebpack (type: Delete){
        delete fileTree('src/main/resources/static/built')
    }
![DeleteBuiltTask](../PART1/images/DeleteBuiltTaskVM.jpg)

Just to be sure that the ***builtt***+ folder was empty I ran the same command *tree* inside the static folder, 
and verified it was actually empty.

![BuiltFolderIsEmpty](../PART1/images/BUILTemptyVM.jpg)

To end this first part of the Class Assignment 3 I added the changes,
committed them closing the issue #33 (above-mentioned) and marked the repository
with the tag **"ca3-part1"**.








