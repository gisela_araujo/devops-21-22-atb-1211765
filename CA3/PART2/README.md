# Class Assignment 3 Report - Part 2

### Virtualization with Vagrant

## 1. Analysis, Design and Implementation

It is important to refer, before starting to expose all the work done during this CA3-Part2, 
that I encounter some problems working with windows and decided to change to linux-ubuntu with which I
worked without having any problems. And this is why, the images presented below some will be exemplifying
the work done using windows and other ubuntu.

First, I downloaded Vagrant into my computer through the link according to class pdf, [here](https://www.vagrantup.com/downloads).
As my Host is Windows I had to do the download for this OS.

Another important subject to refer is the issues created for this Class Assignment, 
which were the following ones:

* Issue #34 : Use Vagrant to setup a virtual environment;
* Issue #35 : Copy Vagrantfile to repository;
* Issue #36 : Update Vagrantfile, so it uses my own gradle version of the spring application ;
* Issue #37 : Replicate the changes in my version of the spring application so it uses the H2 server in the VM;
* Issue #38 : Present an alternative technological solution for the virtualization tool;
* Issue #39 : Add README.md file for CA3 PART2.

After having Vagrant installed we now have in the command line the utilitarian named vagrant.
Then, and in order to check if the version we want is well installed I just ran the following command, 

>vagrant -v

As can be seen below everything was working as expected,

![vagrantInstalled](../PART2/images/1.vagrantVersion.jpg)

### **Use Vagrant to setup a virtual environment**

The main goal for this Part2 of this third Class Assignment is to
use Vagrant to setup a virtual environment to execute the tutorial spring boot application,
gradle "basic" version which was developed in the Part2 of the CA2.

For this part I only copied what is inside the folder named
*react-and-spring-data-rest-basic* which is in the folder *Gradle_Project* in the CA2-Part2.

Before I started working with the gradle project I had to go back to configure Vagrant, as will be
shown below.

#### New VM in 3 Steps 
To test if everything was working as expected I created a project as an example and as was taught in Class 6.

* **STEP 1:**

This first step was to create a folder where I initialized a vagrant project,
for this I used the following command,

>mkdir vagrant-project-1

then, to change to this new created folder I used,

> cd vagrant-project-1

* **STEP 2:**

Then, I had to create a new Vagrant configuration file for the project
, for this I used the following command, 

>vagrant init bento/ubuntu-18.04

I used th 18.04 version because It was the proper one for the Java version I had, which was 11.

To minimize time and setup a vagrant project based on a ubuntu 18.04 box I used the command,

>vagrant box add bento/ubuntu-18.04

These steps are illustrated in the image below.

![createVagrantProjAndInitUbuntu](../PART2/images/2.createVagrantProjectFolderAndInitUbuntu18.04.jpg)

![addVagrantBox](../PART2/images/3.addVagrantBox.jpg)


* **STEP 3:**

For the third and last step, it was time to start the VM,
so I just had to type the command,

>vagrant up

and everything worked as expected.

![VagrantUp](../PART2/images/4.vagrantUpAfteraddVagrantBox.jpg)

Then, and just to be sure how was the status of the virtual environment I typed,

>vagrant status

![Vagrantstatus](../PART2/images/5.vagrantStatus.jpg)

And as can be seen the VM was running.

Then, to start a ssh session to the VM I typed,

>vagrant ssh 

![VagrantSSH](../PART2/images/6.vagrantssh.jpg)

After having Vagrant working correctly as expected, I started doing the Goals for the Class Assignment 3, 
as will be seen below.

#### 1.1.1 Clone from bitbucket 

The first step was to use https://bitbucket.org/atb/vagrant-multi-spring-tut-demo/
as an initial solution.
So, to a folder in my host computer I cloned the file.

> git clone https://Gisela_Araujo@bitbucket.org/atb/vagrant-multi-spring-tut-demo.git

#### 1.1.2 Study the Vagrantfile

Then, I studied the Vagrantfile and saw how it is used to create and provision 2 VMs:
* **web**: this VM is used to run tomcat and the spring boot basic application
* **db**: this VM is used to execute the H2 server database

#### 1.1.3 Copy Vagrantfile into my repository

Then, I copied the Vagrantfile into my repository, in the CA3/Part2 folder.

![VagrantfileCopied](../PART2/images/7.CopyVagrantfile.jpg)

#### 1.1.4 Update Vagrantfile configuration

As referred in the CA3, Part2 slides I configured the Vagrantfile.
First I did change my Repository Settings making my repository public before running my Vagrantfile.

After this I updated my Vagrantfile in specific the command to my repository, as can be seen below,

![VagrantfileUpdated](../PART2/images/8.updateVagrantfileRepository.jpg)

As I installed vagrant bento/ubuntu-18.04 I also had to change the *"vm.box"* in all Vagrantfile.
After this, I encounter some problems when creating the db and web VM's, so I had to do some changes in their ports.
Even though I tried everything for this to work, I encounter even more problems using WindowsShell so I changed to Linux Ubuntu,
as I have DualBoot in my computer, and as will be seen below everything worked as expected.

#### 1.1.5 Replicate changes in my version of the spring application

Next step was to check the repository [here](https://bitbucket.org/atb/tut-basic-gradle)
to see the changes necessary so that the spring application uses the H2 server in the db VM.
And I started replicating the changes in my own version of the spring application.

![ReplicateChangesSpringApp](../PART2/images/9.ReplicateChangesSpringApp.jpg)

Then, I inserted the command,

>vagrant up

and with this I created the db and web vm's.
I had to .gitignore the .vagrant folder in order to not make it occupy a lot of space in my remote repository.

Then checked if the two machines were running, and to see this I had to use the command,

>vagrant status

![VagrantStatus](../PART2/images/10.statusVMs.png)

As can be seen we have two machines running, the **db** and the **web**.
Now, I checked if everything was working with each one of them.

**First, let's start with the **db**:**

I ran the command,

> vagrant ssh db

to start the Virtual Machine.
Then, to see if the H2 server database was being executed I ran the command,

> ps -xa | grep h2

as can be seen below, it was being executed as expected.

![dbVMH2working](../PART2/images/11.dbVMH2Working.png)

Last, I had to check with the URL's if everything was working correctly with the **db**.
Using the README.md file which can be found [here](https://bitbucket.org/atb/vagrant-multi-spring-tut-demo/src/master/),
I could find the correct urls to open.

Open the H2 console using one of the following urls:

* http://localhost:8080/basic-0.0.1-SNAPSHOT/h2-console
* http://192.168.56.10:8080/basic-0.0.1-SNAPSHOT/h2-console

![dbVMH2working](../PART2/images/12.dbWorkingSuccess.png)


![dbVMH2working](../PART2/images/13.dbVMWorkingSelectEmployees.png)

Then, I exited the db and went to the web to see if it was working also as expected.

**Now, let's work with the **web**:**

I ran the command,

> vagrant ssh web

to start the Virtual Machine.
Then, to see if tomcat was being executed I ran the command,

> ps -xa | grep tomcat8

s can be seen below, it was being executed as expected.

![webTomcatWorking](../PART2/images/14.webVMTomcat8Working.png)

Just like with the ***db*** VM I had to check with the URL's if everything was working correctly with the **db**.
Using the README.md file which can be found [here](https://bitbucket.org/atb/vagrant-multi-spring-tut-demo/src/master/),
I could find the correct urls to open.

On the host I opened the spring web application using one of the following options:

* http://localhost:8080/basic-0.0.1-SNAPSHOT/
* http://192.168.56.10:8080/basic-0.0.1-SNAPSHOT/

![webTomcatWorking](../PART2/images/15.webWorkingSuccess.png)

As can be seen, the Frontend and the DataBase were working correctly and as expected.


************************************************************************************************************
## 2. Analysis of an Alternative

When analysing an alternative I found there were plenty of options for Virtualization,
so I decided to try and use VMware, because just like VirtualBox it was compatible with linux.

Throughout this point, I'm going to analyse some pros and cons of using this alternative when comparing it with 
VirtualBox.

But first it's important to refer some information and features of 
these two types of Virtualization Software.

### 2.1. Relevant features of VirtualBox:

* Open source and free;
* Runs on practically any sort of host Operating System;
* Doesn't require the use of a hardware virtualization capability;
* Allows Virtual machine groups;
* Installing and setting up a VirtualBox is easy even for people with lower tech experience;
* It enables users to run numerous operating systems simultaneously on their existing machine
  (Microsoft Windows, Mac OS X, Linux, and Oracle Solaris).

 ####  **Disadvantages:**

* Dependent on the host machine as it uses its hardware. 
  Therefore, the VM will be as efficient and quicker as the Host is.


### 2.2. Relevant features of VMware:

Just like VirtualBox, VMware is a hypervisor software which, after being installed, 
on a physical server allows different O.S. to be operated on the same machine.

#### ***VMware*** offers, for example:

* Virtualization;
* Networking and security management tools;
* Storage software;
* and so on.

#### As main *advantages* we may say VMware has:

* a Virtual Cloud Network, which provides more secure software-defined networking layer;
* the power of being a framework which ties together the business, application and cloud strategies of a user
  to improve **agility** and **performance** in a ***Multi-Could Environment***;
* It allows users to create and run virtual machines (VMs) on a single Windows or Linux desktop. 
  Each VM runs its own O.S allowing the users to run Windows on a Linux machine or vice versa with the native operating system.
* a VMware Site Recovery Manager (SRM), which allows administrators to design recovery plans,
   automatically carried out in the event of a system breakdown.
* it easy when it comes to add a new virtual machine.

#### **Disadvantages:**

* Not being free, we can only used for a few amount of days as trial;
* Low efficiency and lack of reliability.
* Limited in functionality when used for personal and educational uses.
* VMWare contrarily to VirtualBox (which supports a variety of virtual disc types), 
  does not support a large number of different disc formats.

![VMwareVsVirtualBox](../alternative/images/comparision-vmware-vbox.jpg)


### 2.3. Personal Experience:

After using both Hypervisors and even though I encounter some problems in this second part of the assignment using as 
Host Operative System Windows, when I changed to Ubuntu everything worked correctly when it came to use VirtualBox.

When it comes to the installation of the hypervisor I found it easier with VirtualBox, just because there where differente installations
and plugins for VMware. 
For me VirtualBox was also better to create a VM using their interface when comparing both of them. 

But overall, the part of being free would also have a big weight when choosing which one is better.
So, in my personal experience VirtualBox is the way to go when using a Hypervisor.

************************************************************************************************************

## 3. Implementation of the Alternative - using VMware

First I had to install VMWare in my computer, for this I installed the VMware workstation Pro 16 for Ubuntu.
In order to install correctly I followed the instructions presented [here](https://linuxhint.com/vagrant-vmware-workstation-pro-16/).

And everything worked successfully, and the alternative was correctly installed.

Then,  to implement my Virtual Machine now using *VMware* I had to insert the Vagrantfile in a new folder,
in which I made some changes, in order for it to work and create correctly the new VM's.

I used the Vagrantfile that was already working with virtual box.

For the vm.box I used the same one that I used for Virtual Box, because it was compatible with VMware.

The only change I had to do was with:

    # We set more ram memmory for this VM
        web.vm.provider "vmware_workstation" do |v|
            v.memory = 1024
        end

After changing the Vagrantfile for it to work with this new Virtual Machine,
I tried to initialize both VM using the following command,

>vagrant up --provider=vmware_workstation

after this both machines were running as expected,
as can be seen below, by using the command,

>vagrant status

![VMsRunningWithVMware](../alternative/images/vagrantStatusBothVMRunningWithVMware.png)

With the workstation it is possible to see that they're both working as expected,

![VMwareCreated](../alternative/images/webAnddbVMCreatedWithVMware.jpg)


Now using de web VM we can check if everything is working, and it was,

![webWorkingAsExpected](../alternative/images/webWorkingAsExpected.png)

At the end of the part 2 of this assignment I marked my repository with the tag
ca3-part2, and when committing the changes I also closed the issue #38 and #39.






