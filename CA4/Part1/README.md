# Class Assignment 4 Report - Part 1

### **Containers with Docker**

The goal of this assignment is to use Docker to set up a containerized environment and there execute the spring basic tutorial application, already used in several assignments.

For this Class Assignment, Part 1, I created the following issues:

* Issue #40 : Create Dockerfile to build chat server;
* Issue #41 : Create Dockerfile - copy the jar file;
* Issue #42 : Tag the docker image and publish it in a docker hub;
* Issue #43 : README.md file for this Class Assignment 4, Part 1.

## 1. Analysis, Design and Implementation

First, I had to install Docker in my computer. 
As I am using Ubuntu I opened the command line and installed using it.
For the installation I used the following command,

> sudo apt install docker.io

Then, to see the version of docker I ran,

> docker -v

and the version was "Docker version 20.10.12, build 20.10.12-Oubuntu4".

Because, it was made available Docker Desktop for Ubuntu users I also installed that application, using the following link [here](https://docs.docker.com/desktop/linux/install/ubuntu/).

Next, I started working on the Requirements for this Class Assignment.

### 1.1 Create docker images and running containers using the chat application from CA2

#### **1.1.1. Create a docker image to execute the chat server:**

In order to execute the chat server using Docker I created a text document named Dockerfile,
with which Docker can build images automatically using the instructions from it.

> **What is a Dockerfile?**
> 
> As referred before it's a text document which contains the commands for assembling an image.


The main goal of this first part of the class assignment was to create two versions of my solution to
create docker images and run containers using the chat application from CA2.

First, I'll start by referring the first version for which I created a version to build the chat server "inside"
the Dockerfile.

* **1) Chat server built "inside" the Dockerfile:**

For this version I created from scratch a Dockerfile as can be seen below,

![DockerfileV1](../Part1/images/0.DockerFileV1.jpg)

First, I defined with the *FROM* command the version for ubuntu.
Then, I defined the dependencies for the container.

First the java version to be used, in this case the version 11 and the git dependency.
Then we can see the clone command for my private repository.

For the repository I defined my working directory as the one used for the Class Assignment 2 - Part 1,
with the gradle basic demo, being here where the chat server app source code is presented.

Next, the following commands related with gradle wrapper are presented to be run,

* RUN chmod u+x gradlew , command for the gradlew file to have execution permission;
* RUN ./gradlew clean build , the command to build the application.

Lastly,the container communication port is set to 59001 and the chat server is executed with the command,

> ./gradlew runServer

The last *"CMD java -cp build/libs/basic_demo-0.1.0.jar basic_demo.ChatServerApp 59001"* is only executed after 
the docker container is started.

**And how do we start the docker container?**

Well, after defining and saving the Dockerfile in a folder, in the host terminal I had to run the command below,
in order to build the container. So, the Dockerfile created will have the instructions to build the container as we wanted.

> docker build . -t chat_docker_v1

After this the container was built, then I had to start the container, using the following command,

> docker run -p 59001:59001 -d chat_docker_v1

Here, the communication ports for the host and guest were also defined in the port 59001.
And as can be seen below everything worked as expected.

![createContainerV1](../Part1/images/1.createContainerV1.jpg)

Then in the host machine I used the command 

> ./gradlew runClient 
 
to start the application.


![chatServerRunningV1](../Part1/images/2.chatServerRunningContainerV1.jpg)


Just to be sure that the container was running and the chat was working in the guest,
I used the Docker Desktop to see this. And as can be seen in the following image,
the chat server application was running, and everything was working correctly.

![DockerIsRunningInDesktop](../Part1/images/3.DockerDesktopContainersRunning.jpg)


![ChatServerRunningDesktop](../Part1/images/4.ChatServerRunningV1DockerDesktop.jpg)


After this I could close the issue #40.

* **2) Chat server built in the Host Computer and copy the jar file "into" the Dockerfile:**

The second version had the goal of having the built made first in the Host Computer and then copy
the generated jar file, and inserting it into the Dockerfile create the container.

First, I had to stop the running container created for the version 1 "chat_docker_v1", I did this using 
the Docker Desktop application.

Next, I created a new folder inside the CA4-Part1 folder, which I named "v2".
I copied the executable jar file generated with the built I did of my gradle_basic_demo found in my repository.
Here I also added a new Dockerfile. 

![JarAndDockerfile](../Part1/images/5.AddJarAndCreateContainerV2.jpg)


For this new Dockerfile I had to add the required dependencies for what was asked.

So I started to define the *openjdk_11*, pulled from DockerHub.

Then created a new directory named "*/root*" which will be the WORKDIR.

As could be seen below, and for this to work, the jar file as to be in the same directory as the Dockerfile.

And is defined the COPY of this jar file.

As before, it is defined the communication port as 59001.

Lastly, I presented the chat Server command to be executed when the container is started.


![DockerfileV2](../Part1/images/00.DockerFileV2.jpg)


Then, and as was done in the previous version I ran the commands to build and run the new container.
The only variation is the name defined for the image, which had to be different,
and so, it was given the designation of "*chat_docker_v2*".

> docker build . -t chat_docker_v2
> 
> docker run -p 59001:59001 -d chat_docker_v2

![CreateV2Container](../Part1/images/6.CreateV2Container.jpg)

After this, and just like it was done with the first version, in the host I ran the command 
to start the chat application,

> ./gradlew runClient

![ChatServerRunningV2](../Part1/images/8.ChatServerRunningV2Build.jpg)

In the side of the Docker Desktop Application we could also see the chat application running,

![ChatServerRunningV2Desktop](../Part1/images/7.ChatServerRunningV2Container.png)

After this I could close the issue #41.

### 1.2 Tag the image and publish it in docker hub

First I created an account in DockerHub in order to publish the images created and save them.
To save them I also had to create a new repository.

First, I started to search the images I had, and verify if they really existed, 
using the command,

> docker image ls

![listDockerImages](../Part1/images/9.listOfDockerImages.jpg)

As I already had an account created I had to login to DockerHub using the terminal.

![loginDockerHub](../Part1/images/10.loginDockerHub.jpg)

Then I tagged the image, marking the repository I had created, because only after doing this I could push the images.

![tagImage](../Part1/images/11.TagV2Image.jpg)

I opened the Docker Desktop application to be sure the tag was made and that everything was working fine.

![dockerDesktopTagAndLogin](../Part1/images/12.dockerDesktopWithLoginAndTag.jpg)

Lastly, in DockerHub we can see the created images in the repository, with everything saved.
![dockerHub](../Part1/images/13.DockerHubTags.jpg)
![dockerHubDetail](../Part1/images/14.DockerHubTagInDetail.jpg)


To end this first part of the Class Assignment 4 I added the changes,
committed them closing the issue #42 and #43 and marked the repository
with the tag **"ca4-part1"**. 








