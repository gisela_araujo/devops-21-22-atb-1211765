# Class Assignment 4 Report - Part 2

### **Containers with Docker**

The goal of this assignment is to use Docker to set up a containerized environment to execute my version of the gradle
version of the spring basic tutorial application.

For this Class Assignment, Part 1, I created the following issues:

* Issue #44 : Produce a solution similar to the one of CA3-Part2 with Docker;
* Issue #45 : Use docker-compose to produce 2 containers;
* Issue #46 : Publish the images (db and web) to Docker Hub;
* Issue #47 : Use a volume with the db container to get a copy of the database file;
* Issue #48 : Create README.md file for this CA4, part2 and include Dockerfiles in repository;
* Issue #49 : Create alternative for Class Assignment CA4-part2.

## 1. Analysis, Design and Implementation

For this part2 of the class assignment 4 the goal was to produce a solution similar to the one
made in the second the part 2 of the CA3, but now using Docker instead of Vagrant.

For this I used the basic folder in existence iin the CA3-Part2.
Then, the goal which will be explained next is focused on using docker-compose to produce two distinct containers:

* **web**: this container is used to run Tomcat and the spring application;
* **db**: this container is used to execute the H2 server database.

#### 1.1 and 1.2 Use docker-compose to produce 2 services/containers:

From the first part of this CA I already had installed docker and docker desktop, therefore I only had to create
a docker-compose.yml file and two distinct Dockerfiles, one for creating the web service
and the other for the database (db).

To start creating the docker-compose.yml file and the Dockerfiles
for the web and the db I used as resource the teacher's project
[here](https://bitbucket.org/atb/docker-compose-spring-tut-demo/src/master/).
From here I created my own files resorting to my CA3-Part2 vagrant version of the project.

* **DOCKER-COMPOSE FILE :**

> In the *docker-compose* file the only changes I made from the one in the teacher's repository
> was updating the ipv4_address to the one used with vagrant which was: **"192.168,56.11"** .
>
> I also changed the volumes and instead of creating a "data-backup" I change its name only to **"data"**.
>
>Last, because I altered the ipv4 I also had to change the subnet to **"192.168.56.0/24"**.

![docker-composeFile](../Part2/images/000.dockerCompose.png)

* **DOCKERFILE FOR WEB :**

> The Dockerfile for the Web was the file which had to be made more changes.
>
> First I changed the java version to the 11 and not 8.
>
> Then, I cloned my repository and for the **WORKDIR** I inserted the path to
> the basic folder which had the spring-tut-demo in it, from the previously Class Assignment.
>
> In order not to have problems with permissions with gradle wrapper I also added the command
> "chmod u+x gradlew".

![dockerfileWeb](../Part2/images/00.webDockerfile.png)

* **DOCKERFILE FOR DB :**

> In the case of the DB Dockerfile the only change I had to make from the teacher's was in the java's version,
> I used the ***version 11*** just because when I used the version 8 It always caused issues in my computer, and didn't
> work
> correctly.


![dockerfileDB](../Part2/images/0.dbDockerfile.png)

After all these changes it was time to produce the containers.
So, in the CA4 - Part2 folder, I created two different folder,
one named **"db"** and the other **"web"** and in each one of them I inserted the just created Dockerfile.
In the CA4-Part2 folder I inserted the docker-compose file, and here I opened the terminal and executed
the following commands:

> **sudo docker-compose build**


![composeBuildDBWEB](../Part2/images/1.ComposeBuildDBWEB.jpg)

![buildSuccessful](../Part2/images/2.buildSuccessful.jpg)

and then, after the build had been a success,

> **sudo docker-compose up**

![composeUpSuccess](../Part2/images/3.ComposeUpSuccess.jpg)

The images where created and the application was running as can be seen below:

![listImages](../Part2/images/4.listOfImagesDbAndWeb.jpg)

In the url http://localhost:8080/basic-0.0.1-SNAPSHOT/h2-console
the database was working and I was able to see what was saved in the Employee table:

![dbWorking](../Part2/images/5.dbWorking.jpg)

Then, on the browser the link http://localhost:8080/basic-0.0.1-SNAPSHOT I could see the web working:

![webWorking](../Part2/images/6.webWorking.jpg)

#### 1.3 Publish the images (db and web) to Docker Hub

The next step of this class assignment was, as done in part 1, to publish the generated images of the containers
to the Docker Hub Account and Repository.

First, I did the login into my account using the terminal.
And then, in the Docker Hub I created a new repository in which the images would be saved.
To this repository I gave the name **"ca4_part2"**.

After listing the images I tagged the db one and the web and then pushed them to the repo.

I used the following commands in order,

* for the db image tag:

> **sudo docker tag part2_db giselaaraujoswitch/ca4_part2:part2_db**
>
> **sudo docker push giselaaraujoswitch/ca4_part2:part2_db**

* for the web image tag:

> **sudo docker tag part2_web giselaaraujoswitch/ca4_part2:part2_web**
>
> **sudo docker push giselaaraujoswitch/ca4_part2:part2_web**

![pushTagForWeb](../Part2/images/7.PushTagForWeb.jpg)

Then, in my Docker Hub account I could see that the tags were there already and therefore the push was successful.

![dockerHubWithTags](../Part2/images/8.DockerHubRepWithTags.jpg)

In the Docker Desktop applications were also visible the tags created and in which repositories they were.

![dockerDesktopTags](../Part2/images/9.dockerDesktopTagImages.jpg)

#### 1.4 Use a volume with the db container

The next requirement was to use a volume with the db container to get a copy of the database file by using the exec
to run a shell in the container and copying the database file to the volume.
For this I had to access the database container.

First I list the containers for finding the container id for the database.

> **sudo docker ps -a**

next, to access the container in the terminal I inserted the command,

> **sudo docker exec -it c5d478b59469 bash**

and then I was already inside it.

![enterDBContainer](../Part2/images/10.enterDBToCopyFolder.jpg)

Then, to find out if inside the app folder there were any files I listed them
by using the command,

> **ls -la**

![listOfFilesInsideApp](../Part2/images/11.listOfFilesInsideApp.png)

and there were two files:

* h2-1.4.200.jar
* jpadb.mv.db

The goal was to copy this second file to the ./data folder in existence in the host machine.

Inside the container, and as presented in the Dockerfile for the db,
the data folder was hold on the path /usr/src/data,
so the file was copied to that directory's path to be accessible in the host computer.

For this I ran the command,

> **cp ./jpadb.mv.db /usr/src/data**

and as can be seen below it worked and the file was copied to the data folder.

![copiedFileJPADb](../Part2/images/12.copiedFilejpadb.png)

After this, I closed the issues #44, #45, #46 and #47.


************************

## **2. Alternative Solution - Kubernetes**

### Option 1 - Explore Kubernetes:

#### **Kubernetes vs Docker**

Even though we are referring to Kubernetes as an alternative to Docker, we may say this is not an alternative
but more like a complementary solution to Dockers.

So, first, what may we say is Docker?

Primarily, is a suite of software development open source tool for creating, sharing and running individual containers.

On the other hand Kubernetes is a system for operating containerized applications at scale.
Creating these containers is the domain of Docker, and they can run anywhere, on a laptop, in the cloud,
on local servers, and so on.

A modern application consists of many containers.
Operating them in production is the job of Kubernetes.
Since containers are easy to replicate, applications can auto-scale: expand or contract processing capacities to match
user demands.


![differenceKubernetesAndDocker](../Part2/images/Alternative/Difference-Between-Kubernete-and-Docker1-1.jpg)


********************

* **DOCKER Architecture and Components:**

Docker is more than containers, though.
It is a suite of tools for developers to build, share, run and orchestrate containerized apps.

> - Docker build : creates a container image, the blueprint for a container, including everything needed to run an app.
> - Docker Engine : the main docker command line tool which creates and runs containers in the computer.
> - Docker Compose : used in this Class Assignment is a tool for defining and running multi-container apps.
> - Docker Hub : has the role of sharing container images, as a similar function as GitHub.
> - Dockerfile : language for building container images.
> - Docker Swarm : manages a cluster of Docker Engines and here is where Kubernetes start making an appearance,
    overlapping with Docker. Summing it, Docker Swarm mode is a tool for managing containers running on multiple
    servers.


****************************

* **KUBERNETES Architecture and Components:**

It has the main goal of automate the deployment and management of containerized applications.
So, contrary to managing each container individually in a cluster, we can tell
Kubernetes not to allocate the necessary resources in advance.

Kubernetes hides away the details of your servers, and instead provides you with 
a standard API to deploy software onto the cluster.

![kubernetesControlLoop](../Part2/images/Alternative/kuberenetes_as_a-control_loop.png)



>- Container scheduling : based on how busy a server is, kubernetes figures out where the container must be placed.
>- Container management : has the feature of start, stop and restart containers.
>- Auto-scaling containers : if there's high traffic kubernetes can start more containers to handle the load,
>  and then stop them when they're no longer needed.
>- Networking : creates and manages networks to allow containers to communicate with each other. 
>  it also loads balance traffic across several instances of an application.
>- Storage : manages access to disk.
>- Logs and monitoring : makes it easier to gather logs from containers and monitoring their health.
>- With kubernetes we can deploy our own container-based applications.
>- Possible to deploy third-party container-based applications, like databases or web applications (which is the case with the demo of this CA).
>- Connect your apps to each other.
>- Upgrade applications by stopping existing containers and starting new ones with the updated software.
>- Gather metrics on your apps (memory usage, CPU usage, etc.).

The components interact with each other through the API server:

* Master - controls cluster events;
* Cluster - set of nodes grouped together;
* Node - can be a physical or a virtual machine where the containers inside the pods will be launched.
* Pod - logical group of one or more containers which share the same IP address and port.


![componentsOfKubernetes](../Part2/images/Alternative/components-of-kubernetes.jpg)


> **Kubernetes is server-side software for managing containers.**

Dockers and Kubernetes intersect when it comes to container orchestration.
And when it happens we are referring to Docker Swarm and not Docker by itself.

![KvsDockerSwarm](../Part2/images/Alternative/K8s_vs._Docker_Swarm.png)


![kubernetsCluster](../Part2/images/Alternative/kubernetes_cluster.png)


********************

* Differences between Docker and Kubernetes:

| kubernetes                                                                   | Docker Compose                                                                                       | Docker Swarm Mode                                                    |
|------------------------------------------------------------------------------|------------------------------------------------------------------------------------------------------|----------------------------------------------------------------------|
| Manages a cluster of hosts running any compatible container runtime.         | Manages a group of container on a single host.                                                       | Manages a cluster of instances of Docker Engine.                     |
| Can be installed with Docker Desktop by it's a standalone project.           | Comes with Docker Desktop.                                                                           | Build into Docker Engine.                                            |     
| Works with any compatible container runtime.                                 | Works with Docker Engine.                                                                            | Works with Docker Engine.                                            |
| Control with kubectl.                                                        | Control with docker-compose.                                                                         | Control with docker swarm.                                           |
| Cloud integration with providers like AWS.                                   |                                                                                                      |
| Containers can be automatically scaled if they need more resources.          | No auto-scaling.                                                                                     | No auto-scaling.                                                     |
| Many distributions available, like VMWare.                                   | From Docker Inc.                                                                                     | From Docker Inc.                                                     |
| Is a tool for deploying and managing containers on servers on in the cloud.  | Docker is a set of tools for building and running containers, on laptop or on a number of servers.   | Is a Docker's tool for managing containers on a cluster of servers.  |



![kubernetesVsDocker](../Part2/images/Alternative/Kubernetes-vs-Docker-info.jpg)


We may say that for bigger distributed applications and in order to maintain it, managing them and protecting the server of being overused 
then Kubernetes are extremely important.
Like what we have with the basic demo application is not one of these case using only Docker-Compose would be fine and it was.
But we could also apply the use of kubernetes to it.

Next I'll refer how kubernetes could be implemented in this project, but I may say that
when comparing the installation with Docker this was way more complex.


#### **How could it be implemented in this CA?**

**STEP 1 : Enable Kubernetes**

Having Docker Desktop installed in my computer there I enabled Kubernetes, in order for them to start working.

![EnableKubernetes](../Part2/images/Alternative/1.enableKubernets.jpg)

Then, to make it easier to use and develop, I had to install two different tools.

**STEP 2 : Install Minikube**

This may be used with a Docker Container or a Virtual Machine environment.
For this I used as resource the link referring Linux installation [here] (https://minikube.sigs.k8s.io/docs/start/).

![Minikube](../Part2/images/Alternative/2.installMinikube.png)


**STEP 3 : Install Kompose**

Kompose is a conversion tool for Docker Compose (which was used in this CA) to container orchestrators.

I used this link for installation [here](https://kompose.io/).

![Kompose](../Part2/images/Alternative/3.InstallKompose.png)


**STEP 4 : Convert files from the Docker-Compose file using the kompose tool:**

The following step was to convert the docker-compose.yml file using the following command,

>** sudo kompose convert -f docker-compose.yml**

It helped convert my docker compose to container orchestrators, and as can be seen everything worked correctly.

![komposeDockerComposeFile](../Part2/images/Alternative/4.komposeConvert.png)

But then when seeing these new files in the folder I couldn't get permission to edit them and that was important for 
me to continue the implementation of kubernetes in this project.

![finalResultKomposeNewFiles](../Part2/images/Alternative/5.finalresultkompose.png)

These images, after the path was created, could be pushed to a Docker Hub repository and added to be used by the kubernetes.

**STEP 5 : Pods Creation**

After doing the changes above mention to start the Kubernetes cluster I had to start minikube,
for this I used the following command,

>**minikube start**

![MinikubeStart](../Part2/images/Alternative/6.minikubeStart.png)

Then, to get access to the images available on Docker Hub I had to login usign the terminal,

> **docker login**

but, then to create the containers I had to use as resource kubectl, which I hadn't installed yet, so it was time for it.

> **sudo snap install kubectl --classic**

![installKubectl](../Part2/images/Alternative/7.installkubectl.png)

then, on my CA alternative project path I ran the command,

> **kubectl apply -f**

which would apply the changes and create the needed containers.
But this didn't work, and it was from this onward that I couldn't implement kubernetes as an alternative,
and so from now on it will be all theoretically that the implementation with kubernetes will be presented.

After this, in order to see if the containers had been created and were running I would use the command,

>**kubectl get pod -o wide**

and a list of containers and their status would appear in the terminal.
Each pod would have an internal IP address, one for the web and other for the db.

After this there were only small steps remaining, I will refer them now.


**STEP 6 : Copy of database file**

This step refers to the last requirement of the Class Assignment 4, the one in which we had to use a volume with the db 
container to get a copy of the database file by using the exec to run a shell in the container and copying the 
database file to the volume.

To access the database pod I would have to use the following command,

> **kubectl exec -ti (pod name/id) bash**

To copy I only had to used the command,

>**cp ./jpadb.mv.db /usr/src/data**

After this, I could exit the container using the command ***exit*** and to see if the application was running I would 
insert in the terminal the command,

>**minikube service web**

For the database would be,

>**minikube service db** 

Then i could delete the pods and minikube by the end, inserting in the terminal the commands:

> **kubectl delete pod --all**
> 
> and
> 
> **minikube delete**

Overall, I may say I found it easier to implement docker and work with it in this project.
Kubernetes can be used, but they make more sense as a complement to Docker, helping it become more efficient.
I also noticed the difference of commands and the necessity to have more tools installed in order to make it functional
and workable.

By the end of this I closed the two issues still remaining #48 and #49, and marked my repository 
with the tag ***ca4-part2***. 







