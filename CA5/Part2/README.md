# **Class Assignment 5 Report - Part 2**

# **CI/CD Pipelines with Jenkins**

The goal of this second part of the Class Assignment is to practice with Jenkins using the tutorial spring boot application, gradle "basic" version, in my repository - developed in CA2-Part2.

For this Class Assignment, Part 2, I created the following issues:

* Issue #56 : Create Pipeline in Jenkins to build the tutorial spring boot application;
* Issue #57 : Create Jenkinsfile for CA5-Part2;
* Issue #58 : Add Unit Tests to the project;
* Issue #59 : Create a docker image with Tomcat and the war file and publish it in the Docker Hub;
* Issue #60 : Create new docker hub repository for image generated with stage Publish Image;
* Issue #61 : Create username with password key in Jenkins to connect with DockerHub account;
* Issue #62 : Analyze and Implement alternative to CA5;
* Issue #63 : Create README.md for CA5-Part2.


## **1. Analysis, Design and Implementation**

For this part of the assignment it wasn't necessary to install and setup Jenkins as that part was already done before in Part1. But, for this new  assignment which was focused on the spring boot application of the CA2-part2 we had to create a new Jenkinsfile as will be seen below.

#### **1.1 Create new Jenkinsfile**

I started to configure the Jenkinsfile and the Dockerfile in the CA2-Part2 directory but as will be explained next I encounter some problems with the creation of a docker image using Linux and had to re-start this second part of the assignment using windows.

In comparison to the CA5-Part1 Jenkinsfile the stages of ***'Checkout', 'Assemble', 'Test' and 'Archive'*** were the same, the only change I had to make with them was the path of the folder.

Then I had to had new stages which had different purposes, and they were  the following ones:

* ***Javadoc*** : Generates the javadoc of the project and publish it in Jenkins. ( use publishHTML step to archive/publish html reports);

* ***Publish Image*** : Generate a docker image with Tomcat and the war file and publish it in the Docker Hub.  
                        Use docker.build to build an image from a Dockerfile in the same folder as the Jenkinsfile. The tag for the image will be the job build number of Jenkins.


![JenkinnsfileForLinux](../Part2/Images/00.jenkinsFileForLinux.png)

As can be seen above in the stage for Javadoc I was using the publishHTML which was generated using Pipeline Syntax. I had this plugin (*HTML publisher Plugin*) but also the *Javadoc plugin* in Jenkins installed.

![publishHTML](../Part2/Images/1.PublishHTMLScriptJavadoc.png)

In the *'Archiving' stage* as the war file was needed and had to be archived, in order for this to happen I had to add in the build.gradle file the war plugging: 

            plugins {
                id 'war'
            }

Next, in the case of the Docker Image stage I started by having the following commands:


        stage ('Docker Image') {
                    steps {
                        echo 'Publishing Docker Image...'
                        dir("CA2/Part2/Gradle_Projectt/react-and-spring-data-rest-basic"){
                        script {
                            docker.withRegistry('', 'DockerHubCredentials') {
                                def customImage = docker.build("switchGisela/devvops-21-22:${env.BUILD_ID}")

                                customImage.push()
                            }
                        }
                    }
                }


**NOTE** : when I changed to the O.S. Windows I decided to update my Jenkinsfile not only because the command was ***bat*** and not ***ssh***, but I simplified the Javadoc part and as the Docker Image stage was giving me so many problems I decided to also modify it.

##### **1.1.2 Setting the Dockerfile**


For the Image to be created it was also needed a Dockerfile, in this case only for the web application:

> FROM tomcat:8-jdk8-temurin
>
> COPY build/libs/basic-0.0.1-SNAPSHOT.war usr/local/tomcat/webapps/
>
> EXPOSE 8080

##### **1.1.3 Running the Jenkins Pipeline CA5_Part2**


After having everything settled it was time to run the Jenkins pipeline making sure the Docker Desktop was running locally.


![buildInsuccessful](../Part2/Images/000.BuildInsuccessfulWithDockerImage.png)


Everything was working as expected but then when it came to the Docker Image stage I had constantly the same error when looking into the console output, as can be seen below: 

![errorWithDockerImage](../Part2/Images/0.errorWithDockerInLinux.png)

##### **1.1.4 New Pipeline - Trying to make it work now on Windows**


As I couldn't find any solution in order to make this stage work within Linux i changed the Operative System I was using to Windows.

After this I had to install Jenkins again, but then everything was ready to create a new pipeline.

First we have the final and updated Jenkinsfile already with the windows commands and the new directory.

As I was encountering so many problems with the directory for CA2_Part2 I copied it into CA5/Part2/ directory. 


![Jenkinsfile](../Part2/Images/7.jenkinsfileWindows.png)


The stage for the Docker Image was the one I changed and simplified from before just in order to get a success case.


     stage ('Docker Image') {
            steps {
                 script {
                    docker.withRegistry('', 'ga-dockerhub') {
                        def app = docker.build("giselaaraujoswitch/ca5-part2:jenkins-image-${env.BUILD_ID}", 'CA5/Part2/react-and-spring-data-rest-basic')
                        app.push()
                    }
                 }
            }
        }

as we can see I skipped the "echo" part and as i decided to create a new pipeline. I created a new credential, added a new repository in DockerHub where I changed many personal settings trying to make this work, and the path for the repository was also another important change to make.

![dockerHubCredentials](../Part2/Images/5.dockerhubcredentialsForJenkins.png)

I won't get into too many details, but I find important to refer the commands for docker and what they mean:

* ***docker.withRegistry*** : enables access to DockerHub repositories, making it possible to save the Docker image. For which the second argument, with 'ga-dockerhub' is where the Docker credentials created int **Manage Credentials** of Jenkins are added.

* ***docker.build*** : this command is used to build the Docker image, for which th first argument is representative of the remote repository created to save this new created image. And the second argument is for the path where the Dockerfile may be found.

Then after having the Jenkinsfile completed I created a new Pipeline named ca5part2 where I added the path for this new jenkinsffile.

![Pipelineca5part2](../Part2/Images/2.createPipelineWithJenkinsfile.png)


After all the problems above-mentioned, It was time to run the new Jenkins pipeline again, which as will be seen next, occurred without any more problems.


![BuildSuccessful](../Part2/Images/4.BuildSuccessfulInWinfodwsWithDetails.png)


![BuildSuccessfulInDetail](../Part2/Images/3.BuildSuccessfulForCA5Part2InWindows.png)


Then, I checked Docker Hub just to make sure the image was correctly saved, and as can be seen everything worked successfully.


![ImageInDockerHub](../Part2/Images/6.dockerImageInDockerHubRepo.png)


After this, I closed the issues #56, #57, #58, #59, #60 and #61.
And It was time to start looking for one alternative to Jenkins.




************************************************************************************************************

## **2. Analysis of an Alternative - **BUDDY****


### **2.1 Techical Differences - Buddy vs Jenkins**


![buddyVsJenins](../Part2/Images/alternative/buddyVsJenkins.png)

As summarized above and from the experience throughout this Class Assignment Buddy is a more simplified tool, for example it doesn't need any kind of installation.

Other difference between them which makes Buddy a more intuitive and easy tool to use is the fact that the ***Maintenance*** is not made by the user, contrarily to what happens with jenkins. For Jenkins not to crash or fail in a regular basis it is important for the user to have configured it properly.

When it comes to the ***Integration*** we may say Buddy is also easier and better, its plugins don't interfere with one another and quality can be assured centrally. On the other hand, Jenkins also has integrations in the form oof plugins but some of them have been developed by the open source community. Because of this possibility, for the user  it gets better to have an integration safeguarded because if it doesn't exist the plugin may be created by the user. 
But, these plugins may collide with one another contrarily to the buddy ones.

Now to the more important part , the use of **Pipelines**.

With Buddy, we may use the website (which was the case with the implementation of the alternative) or write our own *buddy.yaml* file (this one can also be generated from a *UI generated pipeline*).

As we saw before by using Jenkins, it does have similar options. First, it can be used a UI to create a pipeline (this one not used as much nowadays), secondly we have the Jenkinsfile option which can be used in two different ways - *Declarative* or *Scripted*.


After the build, both of these tools offer a good description of each stage. With Jenkins these stage updates are made almost in real time. 
With Buddy, we also have a live updating screen which shows the output of each stage in our build. 

Both of these tools offer logs for each specific section.


![buddyVsJenins](../Part2/Images/alternative/jenkinsVsbuddy2.png)

In terms of **Pricing** Jenkins is a completely free tool, meaning that the *Setup*, *Subscription* and *Maintenance* are free, not having any licensing costs. Contrary to Jenkins, Buddy charges a monthly subscription, and as referred above this is mostly because of having an experience without crashes not having to be the user setting this all up.

When it comes to customisation by having a vast open source community the new plugins who come out would be at the discretion of the engineer who wrote it and there is no recourse for a critical vulnerability or bug, if there's any problem with it the user has te opportunity to develop their own plugin.

With Buddy and as was said before the customisation options are more limited, the user is not able to build their own customisation, but the available customisations allow the user to have more flexibility with them. When new plugins come out Buddy already checked their vulnerabilities and constantly updates them as part of their support agreement.

When working with Jenkins a user may find it harder and has to have some more knowledge's when it comes to the setup. 
With Buddy, we have a more intuitive way of handling pretty much because of the available UI. Their integrations come with sensible defaults and clear instructions, not needing for a user to have a in-depth knowledge of the Buddy's internal way of working.


### **2.2 Feature Comparison - Buddy vs Jenkins**

Based in this [Feature Comparison](https://buddy.works/compare/jenkins-alternative). 


| ***Features***                 | ***Buddy***                                     | ***Jenkins***  |
|--------------------------------|-------------------------------------------------|----------------|
|**FTP deployment**              | &check;                                         |Limited/Plugin|
|**SFTP deployment**|&check; |Limited/Plugin|
|**Manual Deployment Approval**|&check; |&check; |
|**Sandboxexs (dynamic test environments)**|&check; |&cross;|
|**Database testing and services**|&check;  |Limited/Plugin|
|**Artifacts support**|&check;|&check;|
|**Multi-repo pipeline support**|&check;|&check;|
|**Dependency caching**|&check;|Limited/Plugin|
|**Docker layer cachingg**|&check;|Limited/Plugin|
|**Dedicated support for Android builds and deploy**|&check;|&cross;|
|**Dedicated support for iOS builds and deploy**|&check;|&cross;|
|**Docker environment**|&check;|&check;|
|**Linux VM environment**|&check;|&check;|
|**Windows VM environment**|&check;|&cross;|
|**macOS VM environment**|&check;|&cross;|
|**GUI configuration**|&check;|&check;|
|**YAML configuration**|&check; |Limited/Plugin|
|**Concurrent executions**|&check; |Limited/Plugin|
|**Parallel actions**|&check;|&check;|
|**Techinal support**|&check;|&cross;|
|**Cloud**|&check;|&cross;|
|**On-premises**|&check;|&check;|
|**Trigger on push**|&check;|&check;|
|**Trigger on click**|&check;|&cross;|
|**Trigger on pull request**|&check;|&check;|
|**Trigger on tag**|&check;|&check;|
|**Trigger on specific time (recurrent)**|&check;|&check;|
|**Trigger by another pipeline**|&check;|&check;|
|**Trigger conditions**|&check;|&check;|
|**Team Permissions**|&check;  |Limited/Plugin|
|**Read-only permission**|&check;|&check;|
|**Run-only permission**|&check;|&check;|
|**Security logs**|&check;  |Limited/Plugin|
|**IP restrictions**|&check;|&check;|
|**AWS role assumption**|&check;|&cross;|
|**Dedicated support for AWS services**|&check;|&cross;|
|**Visual tests**|&check;|&cross;|
|**Monitoring Pipelines**|&check;  |Limited/Plugin|
|**Backup Pipelines**|&check;  |Limited/Plugin|
|**Pause and wait for params**|&check;|&check;|
|**Container debug mode**|&check;  |Limited/Plugin|
|**Pipeline grouping**|&check;  |Limited/Plugin|
|**LDAP**|&check;  |Limited/Plugin|
|**Built-in Git hosting**|&check;|&cross;|

### **2.3 PROS and CONS when working with Jenkins vs Buddy**

### **BUDDY**

|**PROS**|**CONS**|
|--------|--------|
|Simple configuration.|Not free, for a better use we have to pay a price.|
|Good integration with all the tools we use and more| Wrong setup won't trigger any notification.|
|UI is simplified and makes the user without any background use it and make a build.| Requires a lot of memory for the self-host variant.|
|It works well with all Operative Systems.||
|Gives Email and Help Desk support to its users.|| 
|Makes available support documentation||


### **JENKINS**

|**PROS**|**CONS**|
|--------|--------|
|Best in the market because of its seamless integration feature|It requires more knowledge on how integration works.|
|Having an open source community which provides solutions for every aspect of Software Developmen Life Cycle.|May have problems in some of the plugins created by this open source community.|
|UI is useful, easy to use and gives logs of everything.| Downloading packages takes a while and some fail randomly without explaining the error.|
|With the console we may see the errors with the buil of the pipeline.| Difficult to troubleshoot some issues with the builds, some errors are uninformative and completely useless.|
|Free.|We have to do an installation which even though it's easy to make takes some time to setup.|


When comparing both of these tools and from my experience with the same project I may say that it was way easier to use Buddy and the problems where non-existent. 
For example when I used Jenkins with Linux I had several errors with the Build of the Image into Docker Hub and then, with Windows, none of these problems appeared.
With Buddy, I used Linux and none of these issues occurred when building and pushing the image as will be demonstrated below with the implementation of the Alternative.


************************************************************************************************************

## 3. Implementation of the Alternative - using Buddy

As referred the alternative chosen for this CA5-part2 was Buddy for which I used the web application [here](https://app.buddy.works).

Here I registered using my Bitbucket account in order to have all my repositories available. 

To make it easier to use, I created another repository which only had the material I was using for CA5/Part2 a [newRepository](https://bitbucket.org/gisela_araujo/ca5_part2_alternative/src/master/), and it is on this one I'm going to work with Buddy.


The first step was to clone the interior of the *react-and-spring-data-rest-basic* into this new repository as can be seen below. There will also be found the Dockerfile needed to build the image.

![1](../Part2/Images/alternative/1.newRepository.png)


With the login I made with my Bitbucket account I was able to have my BitBucket repositories automatically connected to my Buddy account, and I only had to choose with which one I wanted to work on.

To do this I chose my new repository in order to make the build only of this project because I couldn't find a way with these tool to only use a specific folder and not the complete repository.

After this I created a new pipeline and started to add the needed plugins.
First I chose *Gradle* and here I chose the version I needed which was ***"gradle 7.4.1 -jdk11"***.
 Here I inserted the stages I wanted gradle to do, just like I had with the Jenkinsfile, and they were the following ones:

 >gradle build
 >
 >gradle assemble
 >
 >gradle test
 >
 >gradle javadoc

 For the stage of archiving this was not needed, we only had to have in the project the plugin for the war file and this was automatically generated with the build. 

![2](../Part2/Images/alternative/2.stagesForGradle.png)

Having the Gradle plugin already added it was time to choose run the pipeline and make the build. And as can be seen everything passed, meaning the build was successful.

It is important to refer that in this stage I hadn't yet added the Docker plugins and therefore the last needed stage was not up to me run, and this will be made afterwards.

![3](../Part2/Images/alternative/3.BuildSuccessfulOnlyGradle.png)

To see the results of this build and see if it really worked as asked in this Class Assignment we had to go look into the option Filesystem for the different stages.

First we have the test stage, and as can be seen below, Buddy did generate the report files for the tests.

![4](../Part2/Images/alternative/4.Test.png)

The next, and still within the Filesystem I could the reports for the Javadoc stage.

![5](../Part2/Images/alternative/5.JavadocCreated.png)

Next was the Archiving stage which was also present within the Filesystem option, with the war files generated.

![6](../Part2/Images/alternative/6.Archiving.png)

After this, and as I saw that everything was working alright with Gradle it was time to add the Docker part of the work.

So, I added the plugin ***"Build Docker Image"***.

![7](../Part2/Images/alternative/7.AddDockerBuildImage.png)

Here, it was time to choose the Dockerfile I wanted to use in order to build the docker image.

![8](../Part2/Images/alternative/8.selectDockerfile.png)

![9](../Part2/Images/alternative/9.addDockerfile.png)

After this, and in order to save the generated docker image into Docker Hub  it was time to create a new repository in my Docker Hub account.

This new repository dedicated to the alternative implementation I gave the designation of *"ca5-part2-alternative"*.

![10](../Part2/Images/alternative/10.newRepoForAltInDockerHub.png)

Then, I had to go back to the option **"ACTIONS"** to add the plugin needed to push the image. The chosen plugin was ***"Push Docker Image"***.
I only had to set up this plugin for the new repository and my Docker Hub account as will be seen next.

![11](../Part2/Images/alternative/11.ActionsPluginsAdded.png)

So, in the plugin Push Docker Image I had to insert the name of the Docker Hub account in which I wanted to save the generated image. I also inserted the username of my Docker Hub along with the password, and saved this information.

![12](../Part2/Images/alternative/12.dockerHubIntegration.png)

Still with the setup of the repository in Buddy I had to set that the image we wanted to push was the one built previously (the one built with the previous Docker Build Plugin and with the Dockerfile).

The Docker Registry chosen was "Docker Hub" and then I had to choose the Docker Hub account settled above - *ca5-part2-alternative* - and the name of the repository *"giselaaraujoswitch/ca5-part2-alternative"* . Lastly I gave a tag to repository which will appear, after the build, in the Docker Hub repository, and it was *"alternative"*.

![13](../Part2/Images/alternative/13.PushDockerImageToRegistry.png)

Then, after having every requirement of this project done it was time to Run the pipeline and see if the build would be successful as well, after having the Docker part added.

As can be seen below everything went correctly as expected.

![14](../Part2/Images/alternative/14.BuildSuccessfulPassed.png)

![15](../Part2/Images/alternative/15.PipelineSuccess.png)


Then it was time to see if the push of the image was done into the repository and as can be seen it was.

![16](../Part2/Images/alternative/16.dockerImagForAlternativeInRepoDockerHub.png)

After this I could close the issues #62 and #63, and marked my repository 
with the tag ***ca5-part2***. 

Ending this assignment and after the push and tag I saw that the build of the project was failing
and that was because I had this project linked to buddy and it couldn't make build in the pipeline which I forgot to delete.
I maintained the one for the CA5-Part2 project which was compiling and had to delete the other pipeline for the entire repository, 
and that made everything ok.