# Class Assignment 5 Report - Part 1

### **CI/CD Pipelines with Jenkins **

The goal of this first part of the Class Assignment is to practice with Jenkins using the "gradle basic demo" in my repository.

For this Class Assignment, Part 1, I created the following issues:

* Issue #50 : Install/Setup Jenkins;
* Issue #51 : Create a very simple pipeline in Jenkins;
* Issue #52 : Create Pipeline using Jenkinsfile;
* Issue #53 : Build in Jenkins Successful for Option 1;
* Issue #54 : Build in Jenkins Successful for Option 2;
* Issue #55 : Add Jenkinsfile in CA2_Part1/gradle_basic_demo.


## 1. Analysis, Design and Implementation

#### 1.1 Install and Setup JENKINS

First I started by installing Jenkins on my computer. For this I downloaded the war file from [here](https://www.jenkins.io/doc/book/installing/war-file/), and in the folder where I inserted this file I opened the command line to install jenkins.

As I already had the port 8080 in use I had to start the installation defining which port I would use, for this I chose 9090 as can be seen below.

The command to do this was the following one,

>java -jar jenkins.war --httpPort=9090**

![image1](../Part1/Images/1.InstallJenkins.png)

everything worked as expected, and then I could start setting up Jenkins.

Entering the http://localhost:9090/ it was time to create an administrator account with username and password and configure jenkins with recommended plugins.

![image2](../Part1/Images/2.CredentialsJenkkinsPort9090.png)

![image3](../Part1/Images/3.ConfigurationJenkinsURL.png)

After this as we can see the user was successfully created.

![image4](../Part1/Images/4.UserCreated.png)

After this we have two different options of creating a pipeline in which we give Jenkins the instructions to build the project.

#### 1.2 Create Pipeline using Jenkins

The first step was to add an item and there create a Pipeline, which I named "CA5_Part1", 
as can be seen in the image below,

![image5](../Part1/Images/5.CreateNewItemAndPipeline.png)

Then it was time to add the script to create the pipeline.
First the script had the following configuration, 

![image20](../Part1/Images/20.PipelineCA5_Part1.png)

then, I copied the script into the script field in Jenkins:

![image6](../Part1/Images/6.CreateScriptPipeline.png)

After saving the changes I started the build of the gradle_basic_demo project.

The result of the build was successful as expected.
![image7](../Part1/Images/7.BuildSuccessful.png)

In detail, we can see the generated Build Artifacts and the test results of the build.

![image8](../Part1/Images/8.BuildSuccessful1.png)

![image9](../Part1/Images/9.BuildSuccessfulTestDetails.png)

#### 1.3 Create Pipeline using a 'Jenkinsfile'

Here, we have explained the second option of creating a Pipeline.
For this I had to create a ***Jenkinsfile*** with the correct configuration for the build.

The Jenkinsfile was added to the CA2/Part1/gradle_basic_demo, and divided in four different stages, as we've done above.

* First Stage : ***Checkout*** - where we added the git credentialsId for the ssh key and the url.
* Second Stage : ***Assemble*** - for this I used the sh command as I was using linux.
* Third Stage : ***Test*** - also added the junit command for the tests to be added to the build.
* Fourth Stage : ***Archiving*** - in this stage I only added the build/distributions path to the archiveArtefacts feature.

![image21](../Part1/Images/21.JenkinsfileForCA2_Part1.png)


As referred I had to create a ssh key for which the public key will be added to BitBucket and then the private one to Jenkins.

So, in the command line I wrote,

> ssh-keygen

![image11](../Part1/Images/11.generateKeyInCommandLine.png)

After this, a ssh key was generated, and they were added to a new created folder dedicated to save the password.

![image15](../Part1/Images/15.KeyGeneratedWithCommandLine.png)

Then, I went to BitBucket and added a new generated ssh key which I copied from the folder mentioned above *"id_rsa.pub"*.
For this key I gave the name "jenkinsKey". This name was the one added to the Jenkinsfile in the checkout stage,
in concrete with ***"git credentialsId: 'jenkinsKey'"***.

![image14](../Part1/Images/14.AddSSHKeyInBitBucket.png)

Secondly I had to add the second generated key, present in the file *"id_rsa"*, to Jenkins, to this one I gave the name 
***"Gisela_Araujo(Gisela_Araujo_BitBucket_Credenttials)"***.

![image16](../Part1/Images/16.AddSSHKeyJenkins.png)

After having the Jenkinsfile pushed into the repository it was time to create a new pipeline dedicated to make a new 
build in Jenkins.

For this I created a new item,for which I gave the designation "CA2_Part1", and chose Pipeline.

Next, I had to configure the Pipeline, here there were some changes from the previous version.
Going into Pipeline definitions I chose *Pipeline script from SCM*, the SCM chosen was Git.

Then I had to set the Repository, for this I added the URL to my bitbucket repository, and chose the Jenkins credentials
mentioned above.

In the Branches to Build I used the predefined one which was "*/master".

The last configuration step was the Script Path, which was the path for the created Jenkinfile in my repository,
for this, the path was the following one: "CA2/Part1/gradle_basic_demo/Jenkinsfile".

After this I only had to apply and save the changes.

![image19](../Part1/Images/19.CreatePipelineUsingJenkinsfileInBitBucket.png)

In order to build, I went back to this Item - CA2_Part1 - and ran the Build Now option, and everything went correctly as expected.

![image17](../Part1/Images/17.BuildSuccessfulForOption2.png)

In more detail,

![image18](../Part1/Images/18.BuildSuccessfulForOption2Details.png)

As everything went as expected, and successfully, it was time to close the still opened issues and tag my repository
with the tag ***ca5-part1***.







