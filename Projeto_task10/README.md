# Track 10 - Gradle :: Cloud deployment of Web + DB (*PostgreSQL*) with Ansible (ISEP Cloud)

## INTRODUCTION

The main goal of this assignment was to deploy the Web and DB part of our project
to the cloud, in this case the ISEP cloud. For this one important tool was to use Ansible
to create this migration. 

Ansible is used for automation to the provision of the target environment and to 
deploy, after this, the application, in this case the Project Management System created by 
the Group5.

The other tool used was Jenkins, which is used for IT Automation and used for
Continuos Integration and Delivery to provision the target environment.
So, in this track we had Jenkins working together with Ansible as will be seen below.

The last tool used was the database, which in our project we had implemented a H2 database
and so we had to miggrate our data into a PostgreSQL one, which has its differences when
comparing to the one we were using and as will also be reviewed next.

For this track, and as require there were some issues created and assigned to me, 
which were the ones below:

* **Issue 7** : "Task 10 : Create gradle implementation for postgresql";
* **Issue 9** : "Task 10 : Create new application.properties with new database information - set properties for postgresql";
* **Issue 10** : "Task 10 : Create Jenkinsfile";
* **Issue 11** : "Task 10 : Create playbook.config and playbook.run for Ansible";
* **Issue 12** : "Task 10 : Create Vagrantfile and hosts file";
* **Issue 13** : "Task 10 : Create ansible.cfg to configure inventory and remote_user for Ansible";
* **Issue 21** : "Task 10 : Update configuration files to deploy to cloud backend and frontend for ProjectG5";
* **Issue 27** : "Task 10 : Add images referring everything  done while doing task 10";
* **Issue 28** : "Task 10 : Create README.md file for Task10".

## IMPLEMENTATION

### 1 - Add PostgreSQL dependency to Gradle

As we were using in this track a different database we had to install its dependencies
into *build.gradle*.

As can be seen below we had to install the implementation and the dependency for
runTimeOnly for PostegreSQL:

> implementation group: 'org.postgresql', name: 'postgresql', version:'42.4.0'

> runtimeOnly 'org.postgresql:postgresql'

![addDependenciesGradle](../task10/IMAGES/FirstTry/A1.addPostgreSQLBuildGradle.png)


### 2 - Build virtual machine Control with Vagrant and Deploy Jenkins

#### 2.1. Select VM box

For this step and as we already had other tracks creating a Virtual Machine - *"Control"* - using Vagrantfile we adapt 
their Vagrantfile.

With Oracle VirtualBox Provider we looked up which would be the best option.
the latest long-term support (LTS) version of Ubuntu, for now,  we found was Ubuntu 20.04.

Because LTS releases are more stable in comparison to other regular releases and this allows
better integration and compatibility with other pieces of software.

The selected box was ***bento/ubuntu-20.04*** found [here](https://app.vagrantup.com/bento/boxes/ubuntu-20.04).

#### 2.2. Select JDK to install in VM

The Java Project SDK we were working with in our Project and therefore chose to install in our VM
was the Oracle Java SE Development Kit (JDK) 11.

#### 2.3. Create Vagrantfile

As we were doing a cloud deployment for Web and DB with Ansible the Virtual Machines for these would be created
in the ***DEI Virtual Servers Private Cloud*** and they will be configured later
using Jenkins and Ansible.

In the CONTROL VM we installed Jenkins(defined in port:8090), Node.js 12 and Ansible.
So, after creating and start running this VM we could start configuring Jenkins.

Below we can see the Vagrantfile used for all the steps mentioned above.

    Vagrant.configure("2") do |config|
        config.vm.box = "bento/ubuntu-20.04"
        config.vm.provision "shell", inline: <<-SHELL
            sudo apt-get update -y
            sudo apt-get install iputils-ping -y
            sudo apt-get install python3 --yes
            sudo apt-get install openjdk-11-jdk-headless -y
    SHELL

        config.vm.define "control" do |control|
            control.vm.box = "bento/ubuntu-20.04"
            control.vm.hostname = "control"
            control.vm.network "private_network", ip: "192.168.58.10"
            control.vm.provider "virtualbox" do |v2|
                v2.memory = 2048
        end

    # To run ansible from jenkins
        control.vm.synced_folder ".", "/vagrant", mount_options: ["dmode=775,fmode=600"]

    # To access jenkins in port 8090
        control.vm.network "forwarded_port", guest: 8080, host: 8090
      
        control.vm.provision "shell", inline: <<-SHELL
           sudo apt-get update -y
           sudo apt-get install -y curl
           sudo apt-get install apt-transport-https
           sudo apt-get install -y nano git
           sudo apt-get install -y --no-install-recommends apt-utils
           sudo apt-get install software-properties-common --yes
      
    # Install Node
        curl -sL https://deb.nodesource.com/setup_12.x -o nodesource_setup.sh
           sudo bash nodesource_setup.sh
           sudo apt install nodejs

    # Install Ansible
        sudo apt-add-repository --yes --u ppa:ansible/ansible
        sudo apt-get install ansible --yes

    # Install Jenkins
        sudo apt-get install -y avahi-daemon libnss-mdns
        sudo apt-get install -y unzip
        wget https://mirrors.jenkins.io/war-stable/latest/jenkins.war

    # Run Jenkins
       sudo java -jar jenkins.war
      
        SHELL
      end
    end


We ran the command,

> vagrant up

in the directory where the Vagranfile was located, through the command line.

Looking at the Virtual Box application we could see the Control VM fully running.

![ControlVMRunningVirtualBox](../task10/IMAGES/FirstTry/A5.1.VMControlRunninfVirtualBox.png)

Jenkins was correctly installed with the jenkins.war file and was executed in Provision,
given us the password to start configuring it.
It is important to refer that we installed Jenkins as a *super user*, preventing
us from needing a *'Host key verification failed'* error in the future.

![JenkinsPasswordControlVM](../task10/IMAGES/FirstTry/A4.createControlVMOpenJenkins.jpg)


The first time we use the command 'vagrant up' the vagrant environment starts.
On subsequent uses we just need to use this command with the option --provision to force the provisioners
to run and after restart jenkins.

The other option is to start Jenkins entering the CONTROl remote server, running the following commands:

    vagrant up

    vagrant ssh control

    sudo java -jar jenkins.war

![controlVagrantUp](../task10/IMAGES/FirstTry/A5.startVagrantControlUp.png)


### 2- Configure Jenkins

#### 2.1. Post-installation setup

After downloading, installing and running Jenkins,
the post-installation setup wizard begins.


##### 2.1.1. Unlock Jenkins

To ensure Jenkins is securely set up by the administrator, 
admin password is required.

![UnlockJenkins](../task10/IMAGES/FirstTry/A6.loginJenkinsPasswordAdmin.png)

After this we skip the step of creating a user.
We continued as admin, for the login we only needed to
insert *"admin"* as the user and the generated key,
to reenter the jenkins session anytime we want 
as long as the Control is running.

![loginAdminJenkins](../task10/IMAGES/FirstTry/A8.loginAdmin.png)

Lastly, for the Instance Configuration we defined the Jenkins URL as : "localhost:8090".

#### 2.2. Customize Jenkins with plugins

Right after unlocking Jenkins we chose to customize Jenkins installing the base plugins.
We chose "Install suggested plugins" option,
here the plugins found most useful by the Jenkins community are installed.

![BasePluginsJenkinsInstallation](../task10/IMAGES/FirstTry/A7.InstallPluginsJenkins.png)

Next we had to install some plugins required to accomplish successful the requirements for this track.

* **Ansible** plugin:

![AnsiblePlugin](../task10/IMAGES/FirstTry/A12.AnsiblePlugin.png)

* **HTML publisher** plugin:

![HTMLPublisherPlugin](../task10/IMAGES/FirstTry/A13.HTMLpublisher.png)

 * **PostgreSQL** plugins:

![PostgreSQLPlugin](../task10/IMAGES/FirstTry/A14.installPluginForPostGresqlDB.png)

 * **jacoco** plugin:

![jacocoPlugin](../task10/IMAGES/FirstTry/A15.jacocoPluginInstalled_Task10.jpg)

#### 2.3. Create the Jenkinsfile

Before starting a Jenkins project and then create a pipeline we had to, locally, define a 
Jenkinsfile.
This file can be found in the folder created for this track, "DEVOPS/task10/".

Below we can have a look at our Jenkinsfile for track 10:

    pipeline {
        agent any

        stages {
            stage('Check out') {
                steps {
                    echo 'CHECKING OUT...'
                    git credentialsId: 'devops-task10', url: 'https://bitbucket.org/switch-2021/projectg5-devops/'
                }
            }
            stage('Change application.properties file') {
                steps {
                    echo 'CHANGING APPLICATION PROPERTIES FILE...'
                    sh 'sudo rm src/main/resources/application.properties'
                    sh 'sudo cp DEVOPS/task10/application.properties src/main/resources/'
                }
            }

            stage('Change URL_API file') {
                steps {
                    echo 'CHANGING BACKEND URL IN FRONTEND APP...'
                     sh 'sudo rm webapp/src/services/URL_API.js'
                     sh 'sudo cp DEVOPS/task10/URL_API.js webapp/src/services/'
                }
            }
            stage('Setup cloud servers') {
                steps {
                    echo 'SETTING UP CLOUD SERVERS...'
                    ansiblePlaybook credentialsId: 'root', disableHostKeyChecking: true, inventory: 'DEVOPS/task10/hosts', playbook: 'DEVOPS/task10/playbook-configuration.yaml'
                }
            }
            stage('Assemble') {
                steps {
                    echo 'ASSEMBLING...'
                    sh './gradlew assemble'
                }
            }
    
            stage('Javadoc') {
                 steps {
                     echo 'Javadocking'
                     dir('./') {
                      sh './gradlew javadoc'
                      echo 'publishing HTML'
                      publishHTML ([
                         allowMissing: false,
                         alwaysLinkToLastBuild: false,
                         keepAll: false,
                         reportDir: 'build/docs/javadoc/',
                         reportFiles: 'index-all.html',
                         reportName: 'HTML Report',
                         ])
                      }
                 }
            }

            stage('Report') {
                steps {
                    echo 'REPORTING...'
                    sh './gradlew test'
                    junit 'build/test-results/test/*'
                    jacoco exclusionPattern: '**/*Test*.class', execPattern: '**/jacoco.exec', inclusionPattern: '**/*.class'
                }
            }
            stage('Archive') {
                steps {
                    echo 'ARCHIVING...'
                    archiveArtifacts 'build/libs/*.war'
                }
            }
            stage('Build') {
                steps {
                    echo 'BUILDING FRONTEND...'
                    dir('webapp') {
                        sh 'npm install'
                        sh 'sudo npm run build'
                    }
                }
            }
            stage('Deploy to Cloud Servers') {
                steps {
                    echo 'DEPLOYING TO CLOUD SERVERS...'
                    ansiblePlaybook credentialsId: 'root', disableHostKeyChecking: true, inventory: 'DEVOPS/task10/hosts', playbook: 'DEVOPS/task10/playbook-run.yaml'
                }
            }
        }
    }

The Build stages of the Pipeline had to include the following stages:

- ***Assemble***: assemble the application;
- ***Javadoc***: generate and publish javadoc in Jenkins;
- ***Report***: execute tests(unit and integration) and publish its results in Jenkins, including code coverage;
- ***Archive***: publish the distributable artifacts in Jenkins(e.g. jar files), as we were using gradle we used the war files.

Other stages needed for our track:
- ***Check Out***: to enter the repository for ProjectG5_Devops;
- ***Change application.properties file***: as we were using different application properties, specially when it comes to use a different
database - PostgreSQL - we found it easier to have in each folder a different application properties file.
When doing our build, in this stage the general app properties file will be removed and changed to the one in each folder.
- ***Change URL_API file***: change the URL_API for our track;
- ***Setup servers*** : as we had to set up servers in the DEI Cloud this stage was dedicated to run the playboook.configuration 
and use Ansible for this;
- ***Build***: build frontend using npm, installing and running it;
- ***Deploy to Cloud Servers***: send to the cloud, running the front-end of our Project Management System application.


#### 2.4. Create Pipeline in Jenkins for Track 10

Creating a new Item we started a new pipeline to compile and build our ProjectG5_Devops for this track.

The next step was to choose Pipeline as the type of new Item and naming it ***'devops-task10'*** and start working on it.

![CreatePipeline](../task10/IMAGES/FirstTry/A17.createPipelinedevopstask10.png)

##### 2.4.1. Configure the Pipeline - 'devops-task10'


![ConfigurePipeline](../task10/IMAGES/FirstTry/A18.PipelineDetailJenkinsTask10.png)

For this we had to add the repository URL for our Project, and create credentials in BitBucket,
which had to have the same designation as the credentialsId set in the Jenkinsfile - 'devops-task10'.

![bitbucketPass](../task10/IMAGES/FirstTry/A16.createPasswordForJenkinsInBitBucket.png)

After this, it was time to add a jenkins new credential which would connect to git.

![jenkinsPass](../task10/IMAGES/FirstTry/A19.jenkinsCreatePasswordOfBitbucket.jpg)

Lastly was necessary to add the Script Path for the Jenkinsfile, 

    DEVOPS/task10/Jenkinsfile

### 3 - Create Virtual Machines in DEI Cloud for WEB and DB

As we had to do a cloud deployment into ISEP Cloud we had to enter the 
DEI Virtual Servers Private Cloud [here](https://vs-ctl.dei.isep.ipp.pt/)
and create two different machines, one for the WEB and other to deploy the DB.

To create a new VM we chose in the option *"Available Virtual Server Templates (VST)"*
I chose the more general one:

| Number | Virtual Server Template Name | Virtual Server Type |
|--------|------------------------------|---------------------|
|63|Ubuntu 20.04 LTS - base system|9-Docker/Sysbox|

**DB Virtual Machine**

![dbVM](../task10/IMAGES/FirstTry/A11.dbVMCloud.png)

**WEB Virtual Machine**

![WEB_VM](../task10/IMAGES/FirstTry/A10.webVMCloud.png)

**Virtual Machines Running DEI Cloud**

![DB_WEB_VM](../task10/IMAGES/FirstTry/A9.DBandWebVMsOnly.png)


### 4- Configure hosts WEB + DB and deploy web application with Ansible

The first time we configured Ansible and all its related files we only had in consideration using two VM's
in the DEI Cloud, which would be the DB and the WEB, and as will be seen after this, not having separated the backend from the front-end
was a problem.

But this will be specified after referring the first successful build we had and what changes we had to make.

#### 4.1. Create configuration source file (ansible.cfg)

Ansible supports several sources for configuring its behavior, 
including an ini file named ansible.cfg.

Most of Ansible’s settings can be modified using this configuration file to meet the 
needs of our environment, but the default configurations should satisfy most use cases.

    [defaults]
    inventory = /vagrant/hosts
    remote_user = root


#### 4.2. Create inventory file (hosts)

As we only had db and web we had to go to the DEI Cloud VM's a get the information of password and ports in order to create a connection.

Ansible works against multiple managed hosts in our infrastructure at the same time,
using an inventory.

Once our inventory is defined, we use patterns to select the hosts or groups 
we want Ansible to run against.

![DefiningHosts](../task10/IMAGES/FirstTry/A2.hostsOnlyTwoVMs.png)


#### 4.3. Create playbooks

**What's the purpose of using the Ansible Playbooks?**
- Well, they offer a re-usable and simple configuration management and a multi-machine deployment system,
which we needed for our application.
- If we need to execute a task with Ansible more than one time then we can write a playbook
and put it under source control.
- We use Playbooks to push out new configurations or confirm the configuration of remote systems.

**What can Playbooks have and do?**
- Declare configurations;
- do any manual ordered proccess, on multiple machines, in an order which is define in the configuration;
- launch tasks synchronously or asynchronously.

**What did we create and why?**
- playbook-configuration : install all dependencies and software that our VMs should have to suppport our needs.
                           we install the configurations for our PostgreSQL Database in the DB Host,
                           Node.js 12, Apache and Tomcat were first installed in the WEB host.
                           When we realized we had some problems migrating the information from the backend to the frontend
                           we had to break this.
                           Apache was installed in the host for the web, but Tomcat9 was installed in the backend host. 

- playbook-run : this was the playbook to deploy the WEB and DB to the DEI Cloud machines and automate task with Jenkins.

##### 4.3.1- Configure virtual machines: playbook-configuration.yaml

Here we configured the different hosts: servers, db and web.
This was our first try and our first configuration for our playbook config.

We installed Apache Tomcat9 because it requires a Java version 8 or later, as we were using Java11 it was the one
which would work correctly with our project.

    ---

     - hosts: servers
       become: yes

      tasks:
        - name: Update cache
          apt: update_cache=yes

        - name: Remove Apache
          apt: name=apache2 state=absent

        - name: Update cache
          apt: update_cache=yes

        - name: Update package lists
          ansible.builtin.shell:
          sudo apt-get update -y

        - name: Install IP utils
          ansible.builtin.shell:
          sudo apt-get install iputils-ping -y

        # - name: Install Python
        # ansible.builtin.shell:
        # sudo apt-get install python3 --yes

        - name: Install Java
          ansible.builtin.shell:    
          sudo apt-get install openjdk-11-jdk-headless -y

     - name: Install and set up Postgres 14 in Ubuntu 20.04

     hosts: db
     become: yes
     gather_facts: False
     vars:
     postgres_root_user: postgres
     postgres_root_pass: password
     allow_world_readable_tmpfiles: true

    tasks:

        - name: Update apt repo and cache on all Ubuntu boxes
          apt:
          update_cache: yes
          force_apt_get: yes
          cache_valid_time: 3600


        - name: Upgrade all packages on servers
          apt:
          upgrade: dist
          force_apt_get: yes
    
        - name: Install required packages
          apt:
          name:
            - wget
            - python3-psycopg2
            - acl
          state: latest

        - name: Set up Postgres 14 repo
          shell: |
              sudo sh -c 'echo "deb http://apt.postgresql.org/pub/repos/apt $(lsb_release -cs)-pgdg main" > /etc/apt/sources.list.d/pgdg.list'
              wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | sudo apt-key add -
              sudo apt-get update
              sudo apt-get -y install postgresql
              sudo systemctl start postgresql.service
          args:
          warn: no

        - name: Install postgresql
          apt:
          name: postgresql-14
          update_cache: yes
          notify:
            - Enable Postgresql

       - name: Ensure PostgreSQL is listening on *
         lineinfile:
          dest: /etc/postgresql/14/main/postgresql.conf
          regexp: '^listen_addresses\s*='
          line: "listen_addresses='*'"
          state: present
          notify: Restart Postgresql

       - name: Add new configuration to "pg_hba.conf"
         blockinfile:
         dest: /etc/postgresql/14/main/pg_hba.conf
         block: |
             host    all             all             0.0.0.0/0                md5
             host    all             all             ::/0                     md5
         notify: Restart Postgresql

       - name: Change peer identification to trust
         shell: /bin/sed -i '/^local/s/peer/trust/' /etc/postgresql/14/main/pg_hba.conf
         args:
          warn: no
          notify: Restart Postgresql

      - name: Create a Superuser PostgreSQL database user
        become: yes
        become_user: postgres
        postgresql_user:
          name: '{{ postgres_root_user }}'
          password: '{{ postgres_root_pass }}'
          role_attr_flags: CREATEDB,SUPERUSER,CREATEROLE
          encrypted: yes
          state: present
          handlers:

      - name: Restart Postgresql
        systemd:
        name: postgresql
        state: restarted

      - name: Start Postgresql
        systemd:
        name: postgresql
        state: started

      - name: Stop Postgresql
        systemd:
        name: postgresql
        state: stopped

      - name: Enable Postgresql
        systemd:
        name: postgresql
        enabled: yes

    - hosts: web
      become: yes
      tasks:

      - name: Install Node.js 12.18.3
        ansible.builtin.shell: |
        curl -sL https://deb.nodesource.com/setup_12.x -o nodesource_setup.sh
        sudo bash nodesource_setup.sh
        sudo apt install nodejs

      - name: Install Tomcat 9
        apt: name=tomcat9 state=present

      - name: Install Apache
        apt: name=apache2 state=present

      - name: Clean Tomcat
        ansible.builtin.file:
        path: /var/lib/tomcat9/webapps/*
        state: absent

      - name: Install React Router DOM
        ansible.builtin.shell:
        sudo npm install -S react-router-dom
        sudo npm install react-router-dom --save
        sudo npm i react-router-dom --save
        args:
         chdir: /var/www/html/


##### 4.3.2. Deploy Web: playbook-run.yaml

![playbookRunOnlyWEB](../task10/IMAGES/FirstTry/A3.playbookRunOnlyWeb.png)

### 5- Configure PostgreSQL to connect with DB server in cloud

#### 5.1- Setup application.properties in our Java project

We had to be careful inserting the right path to the VM created in the DEI Cloud and define the default port for 
a Postgresql database, which is 5432.
    
    #spring.datasource.url=jdbc:postgresql:mem:testdb
    
    server.servlet.context-path=/switchproject-1.0-SNAPSHOT
    spring.data.rest.base-path=/api
    
    ## //localhost or remote
    spring.datasource.url=jdbc:postgresql://vs858.dei.isep.ipp.pt:5432/postgres
    spring.sql.init.platform = postgres
    spring.datasource.username=postgres
    spring.datasource.password=password
    
    spring.mvc.pathmatch.matching-strategy=ant_path_matcher
    spring.datasource.driverClassName=org.postgresql.Driver
    spring.jpa.database-platform = org.hibernate.dialect.PostgreSQLDialect
    spring.jpa.hibernate.ddl-auto = create-drop
    spring.jpa.properties.hibernate.globally_quoted_identifiers=true
    spring.jpa.defer-datasource-initialization=false
    
    load.bootstrapData=true
    server.port=9090

### 6- Build with Jenkins - First try without Backend Virtual Machine

Having the VM's - DB and WEB - and the CONTROL local VM running we were also able to run Jenkins and 
start the build of the stages.

We found some problems in the installation of the Python version, manually it was installed the the
Python3.8. version using the command line, in all the VM's.

Locally I also had to change the PATH for the Python, to be recognized.

![PythonLocalInstallation](../task10/IMAGES/FirstTry/A20.installPythonLocally.png)

After a few tries we got a successful build.

![buildSuccessfulOnlyWithWEB](../task10/IMAGES/FirstTry/A22.firstSuccessfulBuildOnlyWithDBandWeb.png)

![detailsBuildFirstTry](../task10/IMAGES/FirstTry/A23.buildSucessfulDetails.png)


An insight of the Workspace:

![WorkspaceFirstTry](../task10/IMAGES/FirstTry/A25.workspaceFirstBuildSuccesful.png)


As we can see there were generated test reports as needed.

|[testReports](../task10/IMAGES/FirstTry/A24.testReports.png)

As can be seen the Data was being miggrated but we couldn't load the front-end correctly, and this is why 
we had to change some parts of our work, as will be seen next.

![getProfilesDATA](../task10/IMAGES/FirstTry/A26.GetProfilesExampleDATA.jpg)

![frontendNoDATA](../task10/IMAGES/FirstTry/A27.FrontEndBuiltVMDEI.jpg)

### 7- Add a Backend VM and migrate Data allowing Front-end to Build correctly

#### 7.1. Create VM for Backend in DEI Cloud using the same properties as the others above

![BackendVM](../task10/IMAGES/SecondTryAndFinal/B4.BackendVMcloud.jpg)

So, we now have three VM's in the cloud - DB, WEB and Backend:

![ThreeVMs](../task10/IMAGES/SecondTryAndFinal/B5.ThreeVMsCloud.jpg)

#### 7.2. Update Playbook-run : WEB separated into WEB(front-end) and Backend

![playbookRunWithBackend](../task10/IMAGES/SecondTryAndFinal/B1.updatedPlaybookrunForWeb_Backend_DB.jpg)

#### 7.3. Update HOSTS: Add the new Backend VM created in the DEI Cloud

![hostsWithBackend](../task10/IMAGES/SecondTryAndFinal/B1.UpdateHostsCreatingNewVMForBackend.jpg)

#### 7.4. Update playbook-configuration: Add the new Backend configurations separating some tasks from the WEB

![configWithBAckend](../task10/IMAGES/SecondTryAndFinal/B2.updatePlaybookconfigForBackendAndWeb.jpg)

#### 7.5. JENKINS

We decided to destroy the control VM and start from scratch, so we had to do a new configuration
to Jenkins with a new admin password.

We reinstall the plugins for this task, which were not part of the suggested plugins.

![pluginsSecondTry](../task10/IMAGES/SecondTryAndFinal/B6.pluginManager.jpg)

##### 7.5.1 Create new Pipeline

We decided to maintain the same designations as before, but it was mandatory to create new pipelines,
add again the Jenkinsfile and the Credentials.

![newPipeline](../task10/IMAGES/SecondTryAndFinal/B7.createNewPipelineUpdated.jpg)


Configure Pipeline with Jenkinsfile and credentials:

![newPipelineDetails](../task10/IMAGES/SecondTryAndFinal/B8.PipelineForNewUpdatedConfigurations.jpg)


After having some problems with the last stage in Jenkins we had a successful build,
having the deployment to cloud servers:

![newBuildSuccessBackend](../task10/IMAGES/SecondTryAndFinal/B9.FINALPIPELINEFORDBBACKENDANDWEB.jpg)

![detailsBuildBackend](../task10/IMAGES/SecondTryAndFinal/B10.LASTBUILDWORKING.jpg)

Now we can have a look at the Workspace and seeing that every stage did work as expected:

![WorkspaceWithBackend](../task10/IMAGES/SecondTryAndFinal/B11.workspaceLastBUILD.jpg)

#### 7.6. Database in the DEI Cloud Working

Doing login in the cloud VM for the DB using the command line we could then find the ProjectG5
data loaded.

We defined the password and username to postgreSQL in the application.properties and then in the playbook-configuration
file, and we had to use them in order to login.

The command was the following one:

    # login into "database" postgres

    psql -U postgres --password

![DataInCloud](../task10/IMAGES/SecondTryAndFinal/B12.DATA_postgresql_VisibleInCloud.jpg)

#### 7.7. WEB fully working with Tomcat9 running and DataLoader Initializing Sevlet

![Tomcat9](../task10/IMAGES/SecondTryAndFinal/B13.tomcatDataLoadedInitializingSevlet.jpg)

And now we can see the frontend running and accessible through the Web VM in DEI Cloud:

![FrontendWorking](../task10/IMAGES/SecondTryAndFinal/B14.FrontendUpAndRunningWithTwoVMs.jpg)

### 8. Analysis of the Alternatives

#### 8.1 **Project Management BUILD TOOLS** : Gradle vs Maven

![mavenVsGradle](../task10/IMAGES/Alternatives/MavenVsGradle.jpg)

**MAVEN PROS:**

- Maven is, such as, Gradle a similar and one of the most popular Project Management tool which handles project builds,
 dependencies, distribution, releases, and so on. 
- Therefore, the main goal of using one of these tools is to convert the source code into a stand-alone form that can be run on a computer.
- When considering Maven repository we have a directory of packaged JAR file with pom.xml file. 
  These have the configuration information which allows the building of the project to occur. 
- Furthermore, JAR is a package which combines multiple Java class files and related resources into one file in order to distribute it.
- It is also important to state that Maven's dependencies are in the repository.

**GRADLE PROS:**

- Gradle is an open source build automation system.
  It joins the concepts of Apache Ant and Apache Maven to support multi-project builds.
- Gradle supports the software life cycle from the compilation until packaging and deployment. 
- Allows the appliance of common design principles to the build process. 
- It gives the opportunity of applying various techniques to manage dependencies.

**Differences between tools:**

On one hand Gradle is based on creating a group of task dependencies 
(tasks being the thing that do the work).

On the other hand Maven is based on a more fixed and linear model of phases. 
Here goals are attached to project phases,
and these goals have the same purpose as the tasks in Gradle.

- PERFORMANCE:

In terms of Performance we may say that both allow the multi-module builds to run in parallel However, Gradle allows for incremental builds because it checks which tasks are updated or not. If it is, then the task is not executed, giving you a much shorter build time.

Some performance features that can be found on Gradle and not on Maven are the following ones:

   - Incremental compilations for Java classes;
   - Compile avoidance for Java;
   - The use of APIs for incremental subtasks;
   - A compiler daemon that also makes compiling a lot faster.
   
   
- MANAGING DEPENDENCIES:

- Both Maven and Gradle can handle dynamic and transitive dependencies.
- It's also possible to declare library versions via central versioning definition.
- Both download transitive dependencies from their artifact repositories.
- Both enable us to build only the specified project and its dependencies.

- PERSONAL EXPERIENCE:

When it comes to implement this two Project Management Build tools
we may say that there were no problems implementing both of them.
Even when integrating a new database dependency there were no problems between maven and gradle.

But, for my personal experience, and having to work with Gradle I did find it much easier to use, because:

- Is faster than Maven;
- Easier to customise(the dependencies needed to use for PostgreSQL where, for example, smaller);
- Promotes build code reuse;
- Improves the experience of the user;
- More concise;
- Easy to manipulate.

#### 8.2 **Containers vs Virtual Machines**

With this project we add to use both VMs and Containers to deploy the application.

- **PROS vs CONS**

- When it comes to virtualization virtual machines virtualize an
entire machine down to hardware layers.
- On the other hand containers only virtualize software layers abover the operating system.
- Containers are more lightweight than Virtual Machines, as their images are measured in mb rather than gb.
- Both of them allow developers to improve CPU and memory utilization of physical machines.
- When it comes to enable microservices architectures, Containers also enable them, where 
  application components can be deployed and scaled more granularity.

![ContainerVSVM](../task10/IMAGES/Alternatives/ContainerVSVirtualMachine.png)

In this track it was Virtual Machines which were used to deploy but I did find it harder to work with,
there were slower, took much more of my computer in order to work correctly.

Containers is a much faster way to recreate and modify, when we alter a VM we find
lots of problems, and we have to destroy them time and time again until it works correctly.

Virtual Machines often shut down or crash, this is something we don't have with Containers.

#### 8.3 **PostgreSQL vs H2 Database**

**H2 Database**

- Relational database management system written in Java;
- Can be embedded in Java applications or run in client-server mode;
- It is an open-source lightweight Java database.
- Can be configured to run as inmemory database, which means that data will not persist on the disk.
- Being still a quite recent product there are probably still bugs which weren't found,
and this makes PostgreSQL a tool which is more reliable than H2.
- H2 is more used for development and testing.

**PostgreSQL**

- Powerful open source object-relational database system.
- Reliable, database system with over 30 years;
- Used as the primary data store or data warehouse for many web mobile and analytics applications;
- It is an advanced version of SQL which provides support to different functions of SQL like foreign keys,
subqueries, triggers, and different user-defined types and functions.
- Contrarily to H2 Database in this case we have a more common used production development tool.

Having to migrate data from one type of database to other, and having to configure it using 
the application.properties and the playbook-configuration,
I did see that installing an H2 Database I found it easier.

In order to work correctly PostgreSQL installation  with Ansible does take more commands
and more tasks than installing H2.

For learning purposes and not having to use the console H2 allows a easier way to configure and 
to run as the inmemory database that it is.

## 9. Final considerations / Conclusion

Overall having to develop and integrate different technologies, from Maven to Gradle, Dockers and Virtual Machines,
PostgreSQL and H2, it gives us more experience handling different tools which fill the same purpose
but which have different characteristics and pros vs cons.

PostgreSQL and Ansible where two tools which I had to work with in this Track and learn
from origin.

The first challenge,for all of us working with was Red Hat Ansible.
It was a simple language to work with (yaml), having a data format in their playbooks
easier to understand and implement. 

The biggest challenge in this track was to miggrate data from the database we had already used and implemented,
and which we were comfortable with, H2, and start working with another database, PostgreSQL.

When finding the right implementation it was quick and easy to access data in the Virtual Machine in the 
DEI Cloud. 
Using the command line and the SQL queries we could easily find our data and go from table to table.

Working mostly in pairs when it came to the same task but with different project management 
build tools I found it easier to accomplish the final goal and overcome each and every difficulty we faced.
The all group did take time to discuss each task, sharing ideas, making it even easier to reach a successful end to this work.