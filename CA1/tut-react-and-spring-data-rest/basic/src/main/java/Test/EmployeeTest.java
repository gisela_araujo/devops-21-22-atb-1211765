package Test;

import com.greglturnquist.payroll.Employee;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class EmployeeTest {

    @Test
    void employeeCreationIsSuccessfully() {
        //Arrange
        String firstName = "Bilbo";
        String lastName = "Baggins";
        String description = "ring bearer";
        String jobTitle = "developer";
        int jobYears = 2;
        String email = "bilbo@isep.ipp.pt";

        //Act
        Employee employee = new Employee(firstName,lastName,description,jobTitle,jobYears, email);
        Employee employee1 = new Employee(firstName,lastName,description,jobTitle,jobYears, email);

        //Assert
        assertEquals(employee,employee1);

    }

    @Test
    void employeeCreationIsUnsuccessfullyJobYearsNegative() {
        //Arrange
        String firstName = "Bilbo";
        String lastName = "Baggins";
        String description = "ring bearer";
        String jobTitle = "developer";
        int jobYears = -2;
        String email = "bilbo@isep.ipp.pt";

        //Assert
        assertThrows(NullPointerException.class, () ->
                new Employee(firstName,lastName,description,jobTitle,jobYears, email));

    }

    @Test
    void employeeCreationIsUnsuccessfullyFirstNameNull() {
        //Arrange
        String firstName = null;
        String lastName = "Baggins";
        String description = "ring bearer";
        String jobTitle = "developer";
        int jobYears = 2;
        String email = "bilbo@isep.ipp.pt";

        //Assert
        assertThrows(NullPointerException.class, () ->
                new Employee(firstName,lastName,description,jobTitle,jobYears, email));

    }

    @Test
    void employeeCreationIsUnsuccessfullyLastNameNull() {
        //Arrange
        String firstName = "Bilbo";
        String lastName = null;
        String description = "ring bearer";
        String jobTitle = "developer";
        int jobYears = 2;
        String email = "bilbo@isep.ipp.pt";

        //Assert
        assertThrows(NullPointerException.class, () ->
                new Employee(firstName,lastName,description,jobTitle,jobYears, email));

    }

    @Test
    void employeeCreationIsUnsuccessfullyDescriptionNull() {
        //Arrange
        String firstName = "Bilbo";
        String lastName = "Baggins";
        String description = null;
        String jobTitle = "developer";
        int jobYears = 2;
        String email = "bilbo@isep.ipp.pt";

        //Assert
        assertThrows(NullPointerException.class, () ->
                new Employee(firstName,lastName,description,jobTitle,jobYears, email));

    }

    @Test
    void employeeCreationIsUnsuccessfullyJobTitleNull() {
        //Arrange
        String firstName = "Bilbo";
        String lastName = "Baggins";
        String description = "ring bearer";
        String jobTitle = null;
        int jobYears = 2;
        String email = "bilbo@isep.ipp.pt";

        //Assert
        assertThrows(NullPointerException.class, () ->
                new Employee(firstName,lastName,description,jobTitle,jobYears, email));

    }

    @Test
    void employeeCreationIsUnsuccessfullyEmailNull() {
        //Arrange
        String firstName = "Bilbo";
        String lastName = "Baggins";
        String description = "ring bearer";
        String jobTitle = "developer";
        int jobYears = 2;
        String email = null;

        //Assert
        assertThrows(NullPointerException.class, () ->
                new Employee(firstName,lastName,description,jobTitle,jobYears, email));

    }

    @Test
    void isEmailValid() {

        //Arrange

        String email = "bilbo@isep.ipp.pt";

        //Act

        boolean result = Employee.validateEmail(email);

        //Assert

        assertTrue(result);

    }

    @Test
    void isEmailInvalid() {

        //Arrange

        String email = "bilboisep.ipp.pt";

        //Act

        boolean result = Employee.validateEmail(email);

        //Assert

        assertFalse(result);
    }

}