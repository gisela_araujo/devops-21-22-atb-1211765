# **CA1 Report - Version Control With Git** 

## **1. Analysis, Design and Implementation**

### **1.1. First Week, no branches - 14/03/2022**

In this first week I first started by cloning the professor example repository -
https://github.com/spring-guides/tut-react-and-spring-data-rest - using the terminal, and creating a new repository of my own.

Then, I copied a duplicate of the example with the code of the **"Tutorial React.js and Spring Data REST Application"** into a new folder named **CA1**, as can be seen below.

#### 1.1.1. Operative Guidelines:

To clone the private repository into a local folder in the local machine, the following command was used:
 **git clone https://Gisela_Araujo@bitbucket.org/gisela_araujo/devops-21-22-atb-1211765.git**. 
	

Then, as already referred I created a local folder with the name ***CA1***.

Thirdly, I copied the code of the main application which had already been added to the repository ("Tutorial React.js and Spring Data REST application") into a new folder.

Subsequently, I committed the changes and pushes them, associating this with the tag v1.1.0 (initial version):
 
> **git add .** 
  
> **git commit -m "CA1:  initial commit resolving issue #1"**

> **git tag -a v1.1.0 -m "initial version"**

> **git push origin master v1.1.0** 
 

 Next, we had to work on the five requirements for this week.

#### 1.1.2. First week - requirements/goals

Develop a new feature to add a new field to the application.

In this case, I added a new field to record the years of the employee in the company (e.g. *jobYears*) and added support for the new field: </p>

![AddJobYearsField](tut-react-and-spring-data-rest/basic/images_CA1_project/AddJobYearsField.png)

Added the update of the new field: 		

> **git add .**

Then committed the changes, associating it with and issue:

> **git commit -m "CA1:  add new field jobYears to the application resolving issue #2"** 

Lastly, I pushed the changes:
> **git push**

Then, I added unit tests for testing the creation of Employees and the validation
of their attributes (for instance, no null/empty values). 
For the new field, only integer
values should be allowed.

To run the project I used following command in the intelliJ terminal:

> **./mvnw spring-boot:run**

Then I proceeded to open the link for the application, as seen below:
> https://localhost:8080/

![applicationFrontEnd](tut-react-and-spring-data-rest/basic/images_CA1_project/applicationFrontEnd.jpg)


#### 1.1.2.1. Debug the server and client parts

* **Backend debugging:**

1. In class DataBaseLoader added to the first name field an invalid value (null), tried to run the application,
and it didn't run. <p>
Ex: this.repository.save(new Employee(null, "Baggins", "ring bearer", "developer", 1));

<p></p>


![nullName](tut-react-and-spring-data-rest/basic/images_CA1_project/nullName.jpg)

2. In intelliJ I've put a break point in the Employee constructor, 
to see if everything is ok.
To debug, right click on ReactAndSpringDataRestApplication, and select Debug:

![BackendDebug](tut-react-and-spring-data-rest/basic/images_CA1_project/BackendDebug.jpg)


* **Frontend debugging:**
<p></p>

We also used React Developer Tool for debugging the application in the Frontend side.	

![FrontEndDebug](tut-react-and-spring-data-rest/basic/images_CA1_project/FrontEndDebug.jpg)


**1.1.2.2 Add Changes to BitBucket**

Finally, I committed the changes and pushed them, associating 
***tag v1.2.0 (second version)***.

Added the changes:
> **git add .**

 Committed the changes, associating it to issue number 3:

> **git commit -m "CA1:  Add validations and unit tests for the employees attributes resolving issue #3"**
 
 Tagged the version of the application:

> **git tag -a v1.2.0 -m "second version"**

 Pushed the changes:
> **git push origin master v1.2.0**

 To end this first week assignment I marked my repository with the tag, also adding the readme.md file **ca1-part1**:
 
> **git commit -m "add readme for CA1 and resolve issue #3"**

 then, tagged it with:

> **tag -a ca1-part1 -m "final tag  marking repository**

 finally, I pushed the changes.


### **1.2. Second Week, using branches - 21/03/2022**

For this second part of CA1 I started the assignment by creating
a branch named **"email-field"**.

#### **1.2.1. Create branch - "email-field" branch**

For creating the branch I used the command:

> **git checkout -b email-field**

(in this case, -b stands for creating the branch 
and automatically checking out to the new branch,
the other option for this, would be, 

> **git branch email-field**

> **git checkout email-field**


#### **1.2.2 Add support for the email field**

The visualization in the frontend side of the application can be seen below.
For this, it was created a new field name "email" and 
then there were added values for it in the DataBaseLoader class.

![applicationFrontendWithEmailField](tut-react-and-spring-data-rest/basic/images_CA1_project/applicationFrontendWithEmailField.jpg)


After creating a new field for the email in the application I added the changes and committed them:

> **git add .**

> **git commit -m "Add support for the email field resolving issue #5"**

#### **1.2.3 Testing and Validation of Employees attributes**

The next step was to update the already existing unit tests and add new ones for the creation 
of Employees and the validation of their attributes.
For the new field, null values shouldn't be allowed.
After this, I added and committed the changes into the branch.

> **git add .**

> **git commit -m "Update tests and validation for the email field resolving issue #6"**


#### **1.2.4 Debug the server and client parts**

* **Backend Debug:** 

<p></p>


1. I started the backend debug by putting a break point in the Employee constructor:


![BackendDebugEmployeeConstructor](tut-react-and-spring-data-rest/basic/images_CA1_project/BackendDebugEmployeeConstructor.jpg)

<p></p>

2. I used the ReactAndSpringDataRestApplication in intelliJ, 
changing the email field (in class DataBaseLoader) to null 
to see if, as expected, it didn't run:

![BackendDebugFailedEmailNull](tut-react-and-spring-data-rest/basic/images_CA1_project/BackendDebugFailedEmailNull.jpg)

* **Fronted Debug:**

<p></p>

Using Firefox developer tools I did the frontend debug as seen below:

![FrontendDebugWithEmail](tut-react-and-spring-data-rest/basic/images_CA1_project/FrontendDebugWithEmail.jpg)

#### **1.2.5 Merge code in branch "email-field" with the Master**

After finishing all the work needed in the branch "email-field" it was time
to merge the code with the master and create a new tag for the version of the application.

As I already had been adding my changes and committed them while working (as can be seen above)
I only had to push the changes to the branch,
then proceed to merge it with the master.

> **git push origin email-field**

To merge the email-field branch into master I had to do the following steps:

First I left the email-field branch to the master:
> **git checkout master**

then, already on the master I proceeded to merge the branch
> **git merge email-field**
> **git push**

To tag the commit with the version of the application:
> **git tag -a v1.3.0 -m "v1.3.0"**

Then, I pushed the tag to the remote repository
> **git push --tags**

#### **1.2.6 Create branch for fixing bugs**

For creating the branch called ***"fix-invalid-email"*** I used the command:

> **git checkout -b fix-invalid-email**

(in this case, -b stands for creating the branch and automatically checking out to the new branch,
the other option for this, would be,

> **git branch fix-invalid-email**

> **git checkout fix-invalid-email**

#### **1.2.7 Fix bug for a valid email**

For fixing the bug and therefore making the user insert a valid email in the application
I created a method for validating the email *validateEmail(String email)*, obliging the user to insert an "@".
Then I inserted the validation into the constructor of the Employees.

After this, I created tests to verify if the method created was actually working.

#### **1.2.7.1 Debug the server and client parts**

* **Backend Debug:** 
 
<p></p>

1. I started the backend debug by putting a break point in the Employee constructor already with the validation of email:

![BackendDebugConstructorValidateEmail](tut-react-and-spring-data-rest/basic/images_CA1_project/BackendDebugConstructorValidateEmail.jpg)

2. Another option was to use the ReactAndSpringDataRestApplication in intelliJ,
changing the email field in the DataBaseLoader to an email without an "@" and seeing if it didn't run:

![DebugWithSpringBootInvalidEmail](tut-react-and-spring-data-rest/basic/images_CA1_project/DebugWithSpringBootInvalidEmail.jpg)

* **Fronted Debug:**

Using Firefox developer tools I did the frontend debug as seen below:

![FrontendDebugBugEmailValidation](tut-react-and-spring-data-rest/basic/images_CA1_project/FrontendDebugBugEmailValidation.jpg)

#### **1.2.8 Merge code in branch "fix-invalid-email" with the Master**

After finishing all the work needed in the branch "fix-invalid-email" it was time
to merge the code with the master and create a new tag for the version of the application.
For this part of the assignment I had created an issue "Fix Invalid Email".

First, I had to commit the changes with the validation of email and bugs fixed, and also the updates
made in the documentation part (README.md).

> **git add.**
> **git commit -m "Add validation to email format and update README.md"**

then, I pushed the changes into the branch, 
> **git push origin fix-invalid-email**

to be sure and check all branches, I inserted the following command,

> **git branch -v**

As I did some small changes into the EmployeeTest I proceeded to commit them and closing the 
Issue #7-"Fix Invalid Email".
> **git add .**

> **git commit -m "Add small changes to EmployeeTest, update README and resolving issue #7"**

> **git push origin fix-invalid-email**

To merge the fix-invalid-email branch into master I had to do the following steps:

First I left the fix-invalid-email branch to the master:
> **git checkout master**

then, already on the master I proceeded to merge the branch
> **git merge fix-invalid-email**

> **git push**

To add a tag with the version of the application:
> **git log** (command helpful not only to visualize recent commits
> but also to see each commit ID, date, time, author, and some additional details)

>**git tag -a v1.3.1 -m "v1.3.1"**

Then, I pushed the tag to the remote repository
> **git push --tags**

To end this second part of the assignment I marked my repository with the tag, also adding the readme.md file **ca1-part1**:
First added and committed the changes in the README file with the part of the assignment done with git,

> **git add .**
> 
> **git commit -m "update to README"**

then, marked my repository with the tag ***ca1-part2***:

> **git tag -a ca1-part2 -m "final tag  marking repository"**

finally, I pushed the changes.


************************************************************************************************************
## 2. Analysis of an Alternative

After analysing and doing research on the different possibilities for an alternative to Git,
I chose Mercurial as the alternative to use and analyse.

Mercurial is, such as, Git a similar and one of the most popular Distributed Version Control Systems (DVCS).
With a DVCS the user, in addition to a central repository has, also, one local copy of the repository.
And, both tools use a directed acyclic graph to store history. 
Mercurial is free, open-source and can handle projects of any size.


### Comparisons between Git and Mercurial:

Git is a version management tool used for controlling and modifying the changes in an application and for maintaining the software development cycle.
Mercurial, has the same objective, it's used by developers to manage the application but it is preferred when having a small team working on a project.

In terms of flexibility in version history, the Git tool gives the user the possibility and complete access to change the version history.
On the other side, the Mercurial user doesn't have access to the change it. The user may change the last commit but not the version history.

When using the Git platform it's provided the functionality of a staging area (this is the part where files can be seen that will be there for the next commit).
On the contrary, Mercurial doesn't have that kind of functionality.

The branching mechanism (in use during this CA1, and therefore very important to be analysed) has some differences too between tools.
Git provides a strong branching mechanism, and for one single project a user can create easily one or more branches.
Creating branches using the Mercurial tool is different, it's provided the support for branching but the structure is more complex and not as strong as Git.

One of the main differences between these two tools is that with Git is easier to execute the pull feature, and the push is easier using the Mercurial tool. 

* **Git vs. Mercurial:**

Below we have an image illustrating the main differences, pros and cons between these two tools, Git vs Mercurial:

![GitVsMercurial.jpg](tut-react-and-spring-data-rest/basic/images_CA1_project/Mercurial/GitVsMercurial.jpg)

* **Personal Experience: Git vs. Mercurial:**

After implementing both tools - Git and Mercurial - to the same assignments, I found that even though they have some differences between them, 
I felt comfortable using both of them. Although they are very similar in usage I prefer using Git just because I'm already accustomed to it.

As I saw that the commands for both Git and Mercurial were very similar I decided not to use the terminal with Mercurial.

![commandsForGitAndMercurial.png](tut-react-and-spring-data-rest/basic/images_CA1_project/Mercurial/commandsForGitAndMercurial.png)
(Example and comparison between commands for both Git and Mercurial)



So, in this case I implement the CA1 assignment by using a GUI for Mercurial, which in this case was ***TortoiseHG***. 

    
***TortoiseHG:***
<p>
    In comparison to the use of a terminal, and as expected, the usage of GUI allowed the implementation of the assignment 
    to be easier and more pleasant. 
    The usage of GUI - TortoiseHG - was also a possibility to learn different options of creating repositories as I was accustomed to using the terminal
    to the detriment of using GUI.


************************************************************************************************************
## 3. Implementation of the Alternative: Mercurial

As it was referred above the implementation of Mercurial has been made not by using the terminal but by using a GUI, 
in this case I chose to use TortoiseHG in order to implement the Class Assignment 1 from the beginning.

I started by implementing the first part of the assignment of the first week, so without creating branches.

### 3.1. Mercurial, implementation using TortoiseHG - First Week, no branches:

First I created a folder for the alternative (also named CA1),
then copied the code of the ***Tutorial React.js*** and
***Spring Data REST Application*** into it.


**INITIALIZE:**

After creating a new folder where I copied the code into,
I had to create the new repository.

> Right click on the new folder -> TortoiseHG -> Create Repository here


**COMMIT CHANGES AND TAG VERSIONS:**

* Then, I committed the changes and then marked this version of the application as the initial one, "v1.1.0".

* Using TortoiseHG to ***TAG*** a version is a little different, but simpler:
  - Right click on the commit we want to tag, select the tag option, and choose the name.


![tagInitialVersion.jpg](tut-react-and-spring-data-rest/basic/images_CA1_project/Mercurial/tagInitialVersion.jpg)

Added the validation to the Employee fields and then created unit tests for testing the creation of an Employee.

After this, I committed these changes and added a new tag: v1.2.0

![CommitChangesAndTagV2.jpg](tut-react-and-spring-data-rest/basic/images_CA1_project/Mercurial/CommitChangesAndTagV2.jpg)


### 3.2. Mercurial, implementation using TortoiseHG - Second Week, with branches:

#### 3.2.1. Create Branch named "Email-Field"


**CREATE BRANCHES:**

In the next image we can see how to create a new branch, in this case named ***"email-field"***.

![createBranchEmailField.jpg](tut-react-and-spring-data-rest/basic/images_CA1_project/Mercurial/createBranchEmailField.jpg)


Using intelliJ I added the new email field and its validations.
After this, I committed the changes and was able to then create the new branch in which the changes were added.

![CommitIntoBranchEmailField.jpg](tut-react-and-spring-data-rest/basic/images_CA1_project/Mercurial/CommitIntoBranchEmailField.jpg)
 
 * Here we can see the files that have been changed and will be committed into the new branch. 
 * We can see the commit message which said *"Add new email field to creation of Employee and validation."*.

Then, as instructed in CA1 I inserted unit tests for the email field and committed these changes.


**MERGE BRANCHES:**

1. We need to go back to the default branch. For this we need to use the button (in red below)
for *"Update working directory or switch revisions"*.

![DefaultToMergeBranches.jpg](tut-react-and-spring-data-rest/basic/images_CA1_project/Mercurial/DefaultToMergeBranches.jpg)

2. The merge of the "email-field" branch with the master was next.

For this, I had to right click on the branch I wanted
to merge with the default and click on the option ***"Merge with Local"***

![rightClickOnBranchToMerge.jpg](tut-react-and-spring-data-rest/basic/images_CA1_project/Mercurial/rightClickOnBranchToMerge.jpg)


the merge with the Default Branch:

![mergingEmailFieldBranchWithDefault.jpg](tut-react-and-spring-data-rest/basic/images_CA1_project/Mercurial/mergingEmailFieldBranchWithDefault.jpg)


3. In the end, I tagged this version of the application with "v1.3.0".

![TagVersionWithMergedBranches.jpg](tut-react-and-spring-data-rest/basic/images_CA1_project/Mercurial/TagVersionWithMergedBranches.jpg)


#### 3.2.1. Create Branch named "fix-invalid-email"

For the last part of the assignment I created a new branch, just like before, 
but now designated of "fix-invalid-email". To simplify, I committed the changes in the validation of email, 
adding the new method to validate the creation of an email with an "@". And simultaneously I created the new branch.


**COMMIT AND CREATE A BRANCH:**

In the next image we can see the creation, in this case named ***"fix-invalid-email"***.
Using intelliJ I added the email validation method.
After this, I committed the changes and was able to then create the new branch in which the changes were added.

![commitAndCreateBranchFixInvalidEmail.jpg](tut-react-and-spring-data-rest/basic/images_CA1_project/Mercurial/commitAndCreateBranchFixInvalidEmail.jpg)


Then, as instructed in CA1 I inserted unit tests for validating the email and committed these changes.


**MERGE BRANCHES:**

1. We need to go back to the default branch. For this we need to use the button 
for *"Update working directory or switch revisions"*.

![ReturnToDefaultAfterFixingInvalidEmail.jpg](tut-react-and-spring-data-rest/basic/images_CA1_project/Mercurial/ReturnToDefaultAfterFixingInvalidEmail.jpg)

2. The merge of the "fix-invalid-email" branch with the master was next.

    For this, I had to right click on the branch I wanted
    to merge with the default and click on the option ***"Merge with Local":***

![mergeFixInvalidEmailBranchWithDefault.jpg](tut-react-and-spring-data-rest/basic/images_CA1_project/Mercurial/mergeFixInvalidEmailBranchWithDefault.jpg)


3. In the end, I tagged this version of the application with "v1.3.1".

![taggedWithVersionV1_3_1.jpg](tut-react-and-spring-data-rest/basic/images_CA1_project/Mercurial/taggedWithVersionV1_3_1.jpg)


### 3.3. Create a Remote Repository:

After having created a local repository it was time to insert all this data inside a remote repository.

I created the remote repository in **perforce**, using the ***HelixTeamHub***.

As can be seen in the image below, it is also possible to create issues just like when using BitBucket.

![HelixTeamHub.jpg](tut-react-and-spring-data-rest/basic/images_CA1_project/Mercurial/HelixTeamHub.jpg)

Then, I made push of the local repository and all the changes made in it.

For this there were some steps I had to follow:

     1. Copy the url for the remote repository into TortoiseHG Workbench;
     2. Insert the path in the repository settings;
     3. Then push the changes into the Remote Repository.

![pushChanges.jpg](tut-react-and-spring-data-rest/basic/images_CA1_project/Mercurial/pushChanges.jpg)

The final look of the Remote Repository using Mercurial is presented on the following image.

![RemoteRepositoryFinal.jpg](tut-react-and-spring-data-rest/basic/images_CA1_project/Mercurial/RemoteRepositoryFinal.jpg)




 